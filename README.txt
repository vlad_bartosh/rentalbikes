﻿Перед деплоем war файла сначала настроим DB MySQL, для этого нужно:
1)В файле(...\src\main\resources\db.properties) прописать db.username и db.password к серверу БД.
2)Заполднить БД с помощью скрипта(...\projectDocs\BikeRentalDatabase.sql).

Деплой war файла на TomCat:
1)закидываем war файл и папку rentalBikes(\src\main\webapp\rentalBikes) в папку webapps Tomcat.
2)Запускаем Tomcat(\bin\startup.bat)


По этой ссылке(в зависимости от настроеного порта в TomCat) доступен фронтенд:
http://localhost:<порт>/BicycleRental/rental-bikes/ - на Main страницу



Список маппингов REST API:

http://localhost:<порт>/BicycleRental/bikes (methods: GET,POST)
http://localhost:<порт>/BicycleRental/bikes/{id} (methods: GET,PUT,DELETE)
http://localhost:<порт>/BicycleRental/pickup-points (methods: GET,POST)
http://localhost:<порт>/BicycleRental/pickup-points/{id} (methods: GET,PUT,DELETE)


В папке projectDocs также находится диаграмма классов и модель БД.
