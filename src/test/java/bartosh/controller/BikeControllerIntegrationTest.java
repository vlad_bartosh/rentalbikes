package bartosh.controller;

import bartosh.TestHelper;
import bartosh.config.AppConfig;
import bartosh.config.HibernateConfig;
import bartosh.controller.dto.BikeCreateUpdateDTO;
import bartosh.repository.IBikeEntityRepository;
import bartosh.repository.ICostEntityRepository;
import bartosh.repository.IPickupPointRepository;
import bartosh.repository.IProducerRepository;
import bartosh.repository.entity.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = {HibernateConfig.class, AppConfig.class},
        loader = AnnotationConfigWebContextLoader.class)
public class BikeControllerIntegrationTest {

    @Autowired
    private IBikeEntityRepository iBikeEntityRepository;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ICostEntityRepository iCostEntityRepository;

    @Autowired
    private IPickupPointRepository iPickupPointRepository;

    @Autowired
    private IProducerRepository iProducerRepository;

    private TestHelper testHelper;

    private MockMvc mvc;

    @Before
    public void setupDB() {
        testHelper = new TestHelper(iBikeEntityRepository, iCostEntityRepository, iProducerRepository, iPickupPointRepository);
        testHelper.fillFullDB();
    }

    @Before
    public void setup() {
        mvc = MockMvcBuilders
                .webAppContextSetup(context)
                .dispatchOptions(true)
                .build();
    }

    @Test
    public void getAll() throws Exception {

        mvc.perform(get("/bikes")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getAllPositiveClause() throws Exception {

        mvc.perform(get("/bikes")
                .accept(MediaType.APPLICATION_JSON).param("minCost", "200").param("maxCost", "250")
                .param("offset", "0"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", hasSize(0)))
                .andDo(print());
    }

    @Test
    public void getAllNegativeClause() throws Exception {
        mvc.perform(get("/bikes")
                .accept(MediaType.APPLICATION_JSON).param("minCost", "500").param("maxCost", "400"))
                .andExpect(status().isRequestedRangeNotSatisfiable())
                .andDo(print());
    }

    @Test
    public void getById() throws Exception {
        mvc.perform(get("/bikes/1")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.id").value("1"));
    }

    @Test
    public void addBike() throws Exception {
        TestHelper testHelper = new TestHelper(iBikeEntityRepository, iCostEntityRepository, iProducerRepository, iPickupPointRepository);
        testHelper.fillDBWithoutBikes();
        BikeCreateUpdateDTO bikeDTO = new BikeCreateUpdateDTO(1, 1, TypeBike.HYBRID, Color.GREEN, Status.AVAILABLE_TO_RENT, Sex.FEMALE, AgeType.ADULT,
                1, 3);
        ObjectMapper objectMapper = new ObjectMapper();
        mvc.perform(post("/bikes").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(bikeDTO)))
                .andExpect(status().isCreated());
    }

    @Test
    public void updateBike() throws Exception {
        TestHelper testHelper = new TestHelper(iBikeEntityRepository, iCostEntityRepository, iProducerRepository, iPickupPointRepository);
        testHelper.fillDBWithoutBikes();
        BikeCreateUpdateDTO bikeDTO = new BikeCreateUpdateDTO(2, 1, TypeBike.CITY, Color.ORANGE, Status.UNAVAILABLE_TO_RENT, Sex.MALE, AgeType.ADULT,
                2, 3);
        ObjectMapper objectMapper = new ObjectMapper();
        mvc.perform(put("/bikes/2").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(bikeDTO)))
                .andExpect(status().isOk());
    }


    @Test
    public void deleteBike() throws Exception {
        mvc.perform(delete("/bikes/3"))
                .andExpect(status().is2xxSuccessful());
    }
}
