package bartosh;

import bartosh.controller.dto.BikeDTO;
import bartosh.repository.entity.Sex;
import org.junit.Assert;
import org.junit.Test;

//this test check integration Lombok into the project

public class LombokTest {

    @Test
    public void lombokIntegrationTest() {
        BikeDTO bikeDTO = BikeDTO.builder().sex(Sex.FEMALE).build();
        Assert.assertEquals(Sex.FEMALE, bikeDTO.getSex());
    }
}
