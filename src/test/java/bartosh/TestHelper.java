package bartosh;

import bartosh.repository.IBikeEntityRepository;
import bartosh.repository.ICostEntityRepository;
import bartosh.repository.IPickupPointRepository;
import bartosh.repository.IProducerRepository;
import bartosh.repository.entity.*;

import java.util.Collections;

public class TestHelper {

    private IBikeEntityRepository iBikeEntityRepository;
    private ICostEntityRepository iCostEntityRepository;
    private IProducerRepository iProducerRepository;
    private IPickupPointRepository iPickupPointRepository;

    public TestHelper(IBikeEntityRepository iBikeEntityRepository, ICostEntityRepository iCostEntityRepository, IProducerRepository iProducerRepository, IPickupPointRepository iPickupPointRepository) {
        this.iBikeEntityRepository = iBikeEntityRepository;
        this.iCostEntityRepository = iCostEntityRepository;
        this.iProducerRepository = iProducerRepository;
        this.iPickupPointRepository = iPickupPointRepository;
    }

    public void fillDBWithoutBikes() {
        iCostEntityRepository.insert(new CostEntity("RENT_ADULT_BIKE", 340));
        iCostEntityRepository.insert(new CostEntity("RENT_ADULT_BIKE", 740));
        iCostEntityRepository.insert(new CostEntity("RENT_TEENAGER_BIKE", 440));
        iCostEntityRepository.insert(new CostEntity("RENT_CHILD_HELMET", 2));

        iProducerRepository.insert(new ProducerEntity("Bayer", "Hofenhaim"));
        iProducerRepository.insert(new ProducerEntity("Cabble", "Amsterdam"));
        iProducerRepository.insert(new ProducerEntity("Zubr", "Belarus"));
        iProducerRepository.insert(new ProducerEntity("Bayer", "Berlin"));

        iPickupPointRepository.insert(new PickupPointEntity("Kolas", "Minsk,Kolasa,38", "231", "123123"));
        iPickupPointRepository.insert(new PickupPointEntity("Stepynka", "Minsk,Independent sq. 141", "13131", "1533"));
        iPickupPointRepository.insert(new PickupPointEntity("Malinovka", "Minsk,Dzerzhinskogo,38", "431", "124"));
        iPickupPointRepository.insert(new PickupPointEntity("Sovetskaya", "Minsk,Surganova,78", "2311", "1233"));
    }

    public void fillFullDB() {
        iCostEntityRepository.insert(new CostEntity("RENT_ADULT_BIKE", 340));
        iCostEntityRepository.insert(new CostEntity("RENT_ADULT_BIKE", 740));
        iCostEntityRepository.insert(new CostEntity("RENT_TEENAGER_BIKE", 440));
        iCostEntityRepository.insert(new CostEntity("RENT_CHILD_HELMET", 2));

        iProducerRepository.insert(new ProducerEntity("Bayer", "Hofenhaim"));
        iProducerRepository.insert(new ProducerEntity("Cabble", "Amsterdam"));
        iProducerRepository.insert(new ProducerEntity("Zubr", "Belarus"));
        iProducerRepository.insert(new ProducerEntity("Bayer", "Berlin"));

        iPickupPointRepository.insert(new PickupPointEntity("Kolas", "Minsk,Kolasa,38", "231", "123123"));
        iPickupPointRepository.insert(new PickupPointEntity("Stepynka", "Minsk,Independent sq. 141", "13131", "1533"));
        iPickupPointRepository.insert(new PickupPointEntity("Malinovka", "Minsk,Dzerzhinskogo,38", "431", "124"));
        iPickupPointRepository.insert(new PickupPointEntity("Sovetskaya", "Minsk,Surganova,78", "2311", "1233"));

        iBikeEntityRepository.insert(new BikeEntity(iProducerRepository.getById(1),
                TypeBike.ROAD, Color.BLACK, Status.AVAILABLE_TO_RENT, Sex.FEMALE, AgeType.ADULT,
                iCostEntityRepository.getById(1), iPickupPointRepository.getById(1), Collections.singletonList(new OrderEntity())));
        iBikeEntityRepository.insert(new BikeEntity(iProducerRepository.getById(2),
                TypeBike.ROAD, Color.BLACK, Status.AVAILABLE_TO_RENT, Sex.FEMALE, AgeType.ADULT,
                iCostEntityRepository.getById(2), iPickupPointRepository.getById(2), Collections.singletonList(new OrderEntity())));
        iBikeEntityRepository.insert(new BikeEntity(iProducerRepository.getById(3),
                TypeBike.ROAD, Color.BLACK, Status.AVAILABLE_TO_RENT, Sex.FEMALE, AgeType.ADULT,
                iCostEntityRepository.getById(3), iPickupPointRepository.getById(3), Collections.singletonList(new OrderEntity())));
    }
}
