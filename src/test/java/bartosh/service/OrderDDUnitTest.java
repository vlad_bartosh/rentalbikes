package bartosh.service;

import bartosh.config.AppConfig;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

@WebAppConfiguration
@RunWith(JUnitParamsRunner.class)
@ContextConfiguration(
        classes = AppConfig.class,
        loader = AnnotationConfigWebContextLoader.class)
public class OrderDDUnitTest {

    @Autowired
    private IOrderService iOrderService;

    @ClassRule
    public static final SpringClassRule SPRING_CLASS_RULE = new SpringClassRule();

    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();

    @Test
    @Parameters({
            "3,  0,  3,  0, 9",
            "2,  20, 5,  2, 2.4",
            "5,  15, 6,  3, 4.95",
            "8,  30, 10, 2, 24.6",
            "1,  50, 7,  6, 6.5",
            "14, 12, 5,  3, 8.76"

    })
    public void calculateOrderCostTest(int hours, Double discount, BigDecimal bikesCost, BigDecimal equipmentCost, BigDecimal finalCost) {
        BigDecimal actual = iOrderService.calculateOrderCost(hours, discount, bikesCost, equipmentCost);
        assertEquals(finalCost, actual);
    }
}
