package bartosh.service;

import bartosh.repository.entity.*;
import bartosh.service.model.*;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class FakeBikeDB {
    private List<Bike> persistBikes = new LinkedList<>();

    public FakeBikeDB() {
        List<Order> orders = Arrays.asList(new Order(), new Order(), new Order());
        persistBikes.add(new Bike(new Producer("Bayer", "Hofenhaim"), TypeBike.ROAD, Color.BLACK, Status.AVAILABLE_TO_RENT, Sex.FEMALE, AgeType.ADULT, new PickupPoint(), new Cost(1, "RENT_ADULT_BIKE", 340), orders));
        persistBikes.add(new Bike(new Producer("Lessi", "Japan"), TypeBike.ROAD, Color.YELLOW, Status.UNAVAILABLE_TO_RENT, Sex.MALE, AgeType.CHILD, new PickupPoint(), new Cost(2, "RENT_CHILD_BIKE", 220), orders));
        persistBikes.add(new Bike(new Producer("Chem-in", "China"), TypeBike.CITY, Color.GREEN, Status.AVAILABLE_TO_RENT, Sex.FEMALE, AgeType.ADULT, new PickupPoint(), new Cost(3, "RENT_ADULT_BIKE", 440), orders));
        persistBikes.add(new Bike(new Producer("Karol", "Belgium"), TypeBike.ROAD, Color.RED, Status.AVAILABLE_TO_RENT, Sex.MALE, AgeType.ADULT, new PickupPoint(), new Cost(4, "RENT_ADULT_BIKE", 540), orders));
    }

    public List<Bike> getAllBikes() {
        return persistBikes;
    }

    public Bike getById(int id) {
        return persistBikes.get(id);
    }
}
