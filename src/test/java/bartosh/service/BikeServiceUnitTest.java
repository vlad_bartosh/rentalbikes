package bartosh.service;

import bartosh.repository.entity.BikeEntity;
import bartosh.repository.IBikeEntityRepository;
import bartosh.service.impl.BikeService;
import bartosh.service.model.Bike;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class BikeServiceUnitTest {

    private FakeBikeDB fakeBikeDB = new FakeBikeDB();

    @Mock
    private ModelMapper mapper;

    @Mock
    private IBikeEntityRepository iBikeRepository;

    @InjectMocks
    private BikeService bikeService;

    @Test
    public void getById() {
        Bike bike = fakeBikeDB.getById(1);
        ModelMapper localMapper = new ModelMapper();
        when(iBikeRepository.getById(1)).thenReturn(localMapper.map(bike, BikeEntity.class));
        when(mapper.map(iBikeRepository.getById(1), Bike.class)).thenReturn(bike);
        verify(iBikeRepository).getById(1);
    }

    @Test
    public void insertBike() {
        Bike bike = fakeBikeDB.getById(1);
        ModelMapper localMapper = new ModelMapper();
        when(mapper.map(bike, BikeEntity.class)).thenReturn(new BikeEntity());
        when(iBikeRepository.insert(any(BikeEntity.class))).thenReturn(new BikeEntity());
        when(mapper.map(iBikeRepository.insert(localMapper.map(bike, BikeEntity.class)), Bike.class)).thenReturn(bike);
        assertEquals(bikeService.add(bike), fakeBikeDB.getById(1));
    }


}
