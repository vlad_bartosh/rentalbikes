package bartosh.repository;


import bartosh.config.AppConfig;
import bartosh.config.HibernateConfig;
import bartosh.repository.entity.*;
import bartosh.repository.entity.equipment.HelmetEntity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

import static org.junit.Assert.assertEquals;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = {HibernateConfig.class, AppConfig.class},
        loader = AnnotationConfigWebContextLoader.class)
public class EquipmentEntityIntegrationTest {

    @Autowired
    private IHelmetEntityRepository iHelmetEntityRepository;
    @Autowired
    private ICostEntityRepository iCostEntityRepository;
    @Autowired
    private IProducerRepository iProducerRepository;
    @Autowired
    private IPickupPointRepository iPickupPointRepository;
    private static boolean setUpIsDone;

    @Before
    public void setup() {
        if (setUpIsDone) {
            return;
        }
        iCostEntityRepository.insert(new CostEntity("RENT_ADULT_BIKE", 340));
        iCostEntityRepository.insert(new CostEntity("RENT_ADULT_BIKE", 740));
        iCostEntityRepository.insert(new CostEntity("RENT_TEENAGER_BIKE", 440));
        iCostEntityRepository.insert(new CostEntity("RENT_CHILD_HELMET", 2));

        iProducerRepository.insert(new ProducerEntity("Bayer", "Hofenhaim"));
        iProducerRepository.insert(new ProducerEntity("Cabble", "Amsterdam"));
        iProducerRepository.insert(new ProducerEntity("Zubr", "Belarus"));
        iProducerRepository.insert(new ProducerEntity("Bayer", "Berlin"));

        iPickupPointRepository.insert(new PickupPointEntity("Kolas", "Minsk,Kolasa,38", "231", "123123"));
        iPickupPointRepository.insert(new PickupPointEntity("Stepynka", "Minsk,Independent sq. 141", "13131", "1533"));
        iPickupPointRepository.insert(new PickupPointEntity("Malinovka", "Minsk,Dzerzhinskogo,38", "431", "124"));
        iPickupPointRepository.insert(new PickupPointEntity("Sovetskaya", "Minsk,Surganova,78", "2311", "1233"));

        iHelmetEntityRepository.insert(new HelmetEntity("HelmetDTO Mountain",
                iProducerRepository.getById(1), iCostEntityRepository.getById(1), 31, iPickupPointRepository.getById(1),
                AgeType.ADULT, Color.GREEN));
        iHelmetEntityRepository.insert(new HelmetEntity("HelmetDTO City",
                iProducerRepository.getById(2), iCostEntityRepository.getById(1), 121, iPickupPointRepository.getById(1),
                AgeType.ADULT, Color.GREEN));
        iHelmetEntityRepository.insert(new HelmetEntity("HelmetDTO for kids",
                iProducerRepository.getById(3), iCostEntityRepository.getById(4), 23, iPickupPointRepository.getById(3),
                AgeType.CHILD, Color.GREEN));
        iHelmetEntityRepository.insert(new HelmetEntity("HelmetDTO Mountain",
                iProducerRepository.getById(1), iCostEntityRepository.getById(1), 21, iPickupPointRepository.getById(4),
                AgeType.ADULT, Color.GREEN));


        setUpIsDone = true;
    }


    @Test
    public void getById() {
        HelmetEntity expected = new HelmetEntity("HelmetDTO Mountain",
                iProducerRepository.getById(1), iCostEntityRepository.getById(1), 31, iPickupPointRepository.getById(1),
                AgeType.ADULT, Color.GREEN);
        expected.setId(1);
        HelmetEntity actual = iHelmetEntityRepository.getById(1);
        assertEquals(expected, actual);
    }
}
