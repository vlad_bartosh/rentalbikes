package bartosh.repository;

import bartosh.config.AppConfig;
import bartosh.config.HibernateConfig;
import bartosh.repository.entity.Role;
import bartosh.repository.entity.UserEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = {HibernateConfig.class, AppConfig.class},
        loader = AnnotationConfigWebContextLoader.class)
public class UserEntityIntegrationTest {

    @Autowired
    IUserEntityRepository iUserEntityRepository;

    @Test
    public void getById() {
        Set<Role> roles = new HashSet<>(Collections.singletonList(Role.ROLE_USER));
        UserEntity expected = new UserEntity("Gery", "Gery@gmail.com", "+375294567812", "asdad1123", roles);
        iUserEntityRepository.insert(expected);
        assertEquals(expected, iUserEntityRepository.getById(expected.getId()));
    }
}
