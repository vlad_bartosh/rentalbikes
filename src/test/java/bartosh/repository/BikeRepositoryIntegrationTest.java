package bartosh.repository;

import bartosh.TestHelper;
import bartosh.config.AppConfig;
import bartosh.config.HibernateConfig;
import bartosh.repository.entity.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.AnnotationConfigWebContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = {HibernateConfig.class, AppConfig.class},
        loader = AnnotationConfigWebContextLoader.class)
public class BikeRepositoryIntegrationTest {

    @Autowired
    private IBikeEntityRepository iBikeEntityRepository;
    @Autowired
    private ICostEntityRepository iCostEntityRepository;
    @Autowired
    private IProducerRepository iProducerRepository;
    @Autowired
    private IPickupPointRepository iPickupPointRepository;

    private TestHelper testHelper;

    @Test
    @Rollback
    @Transactional
    public void insertBike() {
        testHelper = new TestHelper(iBikeEntityRepository, iCostEntityRepository, iProducerRepository, iPickupPointRepository);
        testHelper.fillDBWithoutBikes();
        BikeEntity insrBike = new BikeEntity();
        insrBike.setProducer(iProducerRepository.getById(1));
        insrBike.setTypeBike(TypeBike.ROAD);
        insrBike.setColor(Color.BLACK);
        insrBike.setStatus(Status.AVAILABLE_TO_RENT);
        insrBike.setSex(Sex.FEMALE);
        insrBike.setAgeType(AgeType.ADULT);
        insrBike.setCostEntity(iCostEntityRepository.getById(1));
        insrBike.setPickupPointEntity(iPickupPointRepository.getById(1));
        iBikeEntityRepository.insert(insrBike);
        BikeEntity actual = iBikeEntityRepository.getById(insrBike.getId());
        assertEquals(insrBike, actual);
    }

}
