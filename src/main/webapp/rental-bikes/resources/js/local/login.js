$(document).ready(function () {
    $('#loginButton').click(function () {
        $('#error-login').text("");
        var login = $('#login').val();
        var password = $('#password').val();
        $.ajax({
            url: hostUrl + '/user/login',
            contentType: "application/json",
            method: "GET",
            data: {
                "login": login,
                "password": password
            },
            success: function (user) {
                var authToken = make_base_auth(login, password);
                var localStorageObject = {
                    "idUser": user.id,
                    "authHeader": authToken
                };
                console.log("Data: " + user.login + "\nStatus: " + status);
                localStorage.setItem("rentalBikes", JSON.stringify(localStorageObject));
                window.location.href = '/rental-bikes/index.html';
            },
            error: function (error) {
                var error_msg = error.responseJSON.message;
                $('#error-login').text(error_msg);
            }
        });
    })


});

function make_base_auth(user, password) {
    var tok = user + ':' + password;
    var hash = window.btoa(tok);
    return "Basic " + hash;
}