$.tmplLoader.ready(function () {
    $(document).ready(function () {
        if (localStorage.length !== 0) {
            var user = JSON.parse(localStorage.getItem(localStorage.key(0)));
            $('#mainPage').attr('href', 'index.html');
            $.get(hostUrl + "/rental-bikes/templates/right-logout.tmpl.html", function (data) {
                $("#right-bar").html(data);
            });
            $.ajax({
                url: hostUrl + '/orders/user',
                contentType: "application/json",
                method: "GET",
                data: {
                    user_id: user.idUser
                },
                success: function (orders) {
                    var finalCost = 0;
                    for (var i = 0; i < orders.length; i++) {
                        finalCost += orders[i].finalCost;
                    }
                    $('#userHello').text(orders[0].user.login + ", your order is accepted. Final cost: " + finalCost + "$");
                    $('#right-bar').find('#logoutButton').click(function () {
                        localStorage.removeItem(localStorage.key(0));
                        window.location.href = '/rental-bikes/index.html';
                    })
                }
            });
        } else {
            $('#userHello').text("Hello,visitor! Welcome to RentalBikes.");
            $.get(hostUrl + "/rental-bikes/templates/right-sign-up.tmpl.html", function (data) {
                $("#right-bar").html(data);
            });
        }
    });
});
