$.tmplLoader.ready(function () {
    $(document).ready(function () {
        $('.input-group.date').datetimepicker({
            format: 'YYYY-MM-DD HH:mm',
            locale: 'ru'
        });
        $.when(
            $.ajax({
                url: hostUrl + "/pickup-points",
                method: "GET",
                success: function (pickupPoints) {
                    console.log(pickupPoints);
                    fillPickupOptions(pickupPoints);
                    fillPickoffOptions(pickupPoints);
                }
            }),
            nextButtonClick(function (pickupPointId, pickoffPointId, dateFrom, dateTill) {
                $.get(hostUrl + "/rental-bikes/templates/right-filterEquipments.tmpl.html", function (data) {
                    $("#filterEquipment").html(data);
                    fillEquipment(pickupPointId, function () {
                        cleanOptions(function () {
                            fillDefaultValuesSelect(function () {
                                bikesTable(pickupPointId, pickoffPointId, dateFrom, dateTill, 0, 6);
                                filterBikeTable(pickupPointId, pickoffPointId, dateFrom, dateTill);
                            });
                        });
                    });
                })
            })
        );
    })
});

function nextButtonClick(callback) {
    $.when($('#next').click(function () {
        $('#error-dates').text("");
        var pickupPointId = $("#pickupPoint").children(":selected").attr("id");
        var pickoffPointId = $("#pickoffPoint").children(":selected").attr("id");
        var rentFrom = $("#rentFrom").val();
        var rentTill = $("#rentTill").val();
        if (rentFrom == "") {
            $('#error-dates').text("You should fill all fields");
            return;
        }
        if (rentTill == "") {
            $('#error-dates').text("You should fill all fields");
            return;
        }
        var currentDate = new Date();
        if (new Date(rentFrom) < currentDate) {
            $('#error-dates').text("Incorrect dates");
            return;
        }
        var diffHours = parseInt((new Date(rentTill) - new Date(rentFrom)) / (1000 * 60 * 60 ));
        if (diffHours <= 0) {
            $('#error-dates').text("Incorrect dates");
            return;
        }
        $.get(hostUrl + "/rental-bikes/templates/left-filterBikes-bar.tmpl.html", function (data) {
            $("#filterBikes").find(".col-xs-6.col-sm-12").html(data);
            $.ajax({
                url: hostUrl + '/bikes',
                contentType: "application/json",
                method: "GET",
                data: {
                    pickupPoint_id: pickupPointId,
                    offset: 0
                },
                success: function (bikes) {
                    console.log(bikes);
                    fillBikeOptions(bikes);
                    callback(pickupPointId, pickoffPointId, rentFrom, rentTill);
                },
                error: function (error) {
                    var error_msg = error.responseJSON.message;
                    $('#error-dates').text(error_msg);
                },
                dataType: "json"
            })
        })
    }));
}

function fillEquipment(pickupPointId, callback) {
    fillBasket(pickupPointId, function () {
        fillBabySeats(pickupPointId, function () {
            fillHelmets(pickupPointId, function () {
                callback();
            })
        })
    })
}


function fillBikeOptions(bikes) {
    for (var i = 0; i < bikes.length; i++) {
        $('.col-xs-6.col-sm-12 #producer').append($("<option></option>").attr("id", bikes[i].id).attr("value", bikes[i].producer.name).text(bikes[i].producer.name));
        $('.col-xs-6.col-sm-12 #typeBike').append($("<option></option>").attr("value", bikes[i].typeBike).text(bikes[i].typeBike));
        $('.col-xs-6.col-sm-12 #color').append($("<option></option>").attr("value", bikes[i].color).text(bikes[i].color));
        $('.col-xs-6.col-sm-12 #ageType').append($("<option></option>").attr("value", bikes[i].ageType).text(bikes[i].ageType));
    }
}

function fillPickupOptions(pickupPoint) {
    for (var i = 0; i < pickupPoint.length; i++) {
        $('.col-xs-6.col-sm-12 #pickupPoint').append($("<option></option>").attr("id", pickupPoint[i].id).attr("value", pickupPoint[i].name).text(pickupPoint[i].name));
    }
}

function fillPickoffOptions(pickoffPoint) {
    for (var i = 0; i < pickoffPoint.length; i++) {
        $('.col-xs-6.col-sm-12 #pickoffPoint').append($("<option></option>").attr("id", pickoffPoint[i].id).attr("value", pickoffPoint[i].name).text(pickoffPoint[i].name));
    }
}

function cleanOptions(callback) {
    var found = [];
    $("select option").each(function () {
        if ($.inArray(this.value, found) !== -1) $(this).remove();
        found.push(this.value);
    });
    callback();
}

function fillDefaultValuesSelect(callback) {
    $("select").each(function () {
        $(this).prepend("<option selected></option>")
    });
    callback();
}

function bookClick(pickupPointId, pickoffPointId, dateFrom, dateTill) {
    $("#bikeTableBody").find(".btn-outline-success").click(function () {
        var user = JSON.parse(localStorage.getItem(localStorage.key(0)));
        if (user == undefined) {
            window.location.href = '/rental-bikes/login.html';
        }
        var idBike = $(this).parent().parent().attr("id");
        var pickupPoint_id = pickupPointId;
        var pickoffPoint_id = pickoffPointId;
        var rentFrom = dateFrom;
        var rentTill = dateTill;
        var equipments_id = [];
        var helmet_id = $("#helmet").children(":selected").attr("id");
        if (helmet_id != undefined) {
            equipments_id.push(helmet_id);
        }
        var babySeat_id = $("#babySeat").children(":selected").attr("id");
        if (babySeat_id != undefined) {
            equipments_id.push(babySeat_id);
        }
        var basket_id = $("#basket").children(":selected").attr("id");
        if (basket_id != undefined) {
            equipments_id.push(basket_id);
        }
        var user_id = user.idUser;
        var orderDTO = {
            "pickupPoint_id": pickupPoint_id,
            "pickoffPoint_id": pickoffPoint_id,
            "rentFrom": rentFrom,
            "rentTill": rentTill,
            "bikes_id": [idBike],
            "helmet_id": helmet_id,
            "basket_id": basket_id,
            "babySeat_id": babySeat_id,
            "user_id": user_id,
            "finalCost": null,
            "discount": 0,
            "id": null
        };
        $.ajax({
            beforeSend: function (xhr) {
                if (localStorage.length !== 0) {
                    var user = JSON.parse(localStorage.getItem(localStorage.key(0)));
                    xhr.setRequestHeader('Authorization', user.authHeader);
                }
            },
            url: hostUrl + '/orders',
            contentType: "application/json",
            method: "POST",
            data: JSON.stringify(orderDTO),
            success: function (order) {
                console.log("Data: " + order);
                console.log("Status: " + status);
                sendOrderToEmails(order);
                window.location.href = '/rental-bikes/orderAccepted.html'
            },
            error: function (error) {
                if (error.status == 401) {
                    window.location.href = '/rental-bikes/login.html'
                }
                console.log(error);
            },
            dataType: "json"
        });
    })
}

function paginationBikes(pickupPointId, pickoffPointId, dateFrom, dateTill, queryParam) {
    $.ajax({
        url: hostUrl + '/bikes/rows',
        method: "GET",
        data: queryParam,
        success: function (rowsCount) {
            var limit = queryParam.limit;
            var totalPages = 1;
            if (rowsCount != 0) {
                totalPages = Math.ceil(rowsCount / limit);
            }
            $('#pagination').twbsPagination({
                totalPages: totalPages,
                visiblePages: 2,
                onPageClick: function (event, page) {
                    var offset = 0;
                    if (page != 1) {
                        offset = (page - 1) * limit;
                    }
                    bikesTable(pickupPointId, pickoffPointId, dateFrom, dateTill, offset, limit);
                }
            });
        }
    });

}

function bikesTable(pickupPointId, pickoffPointId, dateFrom, dateTill, offset, limit) {
    var queryParam = {
        pickupPoint_id: pickupPointId,
        producer: $("#producer").val(),
        typeBike: $("#typeBike").val(),
        color: $("#color").val(),
        sex: $("#checkMale").is(':checked') && $("#checkFemale").is(':checked') ? {} :
            $("#checkMale").is(':checked') ? $("#checkMale").val() :
                $("#checkFemale").is(':checked') ? $("#checkFemale").val() : {},
        ageType: $("#ageType").val(),
        minCost: $("#minCost").val(),
        maxCost: $("#maxCost").val(),
        offset: offset,
        limit: limit
    };
    $.ajax({
        url: hostUrl + '/bikes',
        method: "GET",
        data: queryParam,
        success: function (bikes) {
            $('#error-filter').text("");
            var content = $.tmplLoader('bikesTable', {bikes: bikes});
            $("#centerContainer").find('#bikeTableBody').html(content);
            bookClick(pickupPointId, pickoffPointId, dateFrom, dateTill);
            paginationBikes(pickupPointId, pickoffPointId, dateFrom, dateTill, queryParam);
        },
        error: function (error) {
            var error_msg = error.responseJSON.message;
            $('#error-filter').text(error_msg);
        }
    });
}

function filterBikeTable(pickupPointId, pickoffPointId, dateFrom, dateTill) {
    $.when($(".col-xs-6.col-sm-12 #left-bar").find("#findBikes").click(function () {
        $('#pagination').twbsPagination('destroy');
        bikesTable(pickupPointId, pickoffPointId, dateFrom, dateTill, 0, 6);
    }));
}


function fillHelmets(pickupPoint_id, callback) {
    return $.ajax({
        url: hostUrl + '/helmets',
        method: "GET",
        success: function (helmets) {
            var filterHelmets = helmets.filter(function (helmet) {
                return (helmet.pickupPointDTO.id == pickupPoint_id);
            });
            $("#filterEquipment").find("#helmet").html($.tmplLoader('selectHelmet', {helmets: filterHelmets}));
            callback();
        }
    });
}

function fillBasket(pickupPoint_id, callback) {
    return $.ajax({
        url: hostUrl + '/baskets',
        method: "GET",
        success: function (baskets) {
            var filterBaskets = baskets.filter(function (basket) {
                return (basket.pickupPointDTO.id == pickupPoint_id);
            });
            $("#filterEquipment").find("#basket").html($.tmplLoader('selectBasket', {baskets: filterBaskets}));
            callback();
        }
    });
}

function fillBabySeats(pickupPoint_id, callback) {
    return $.ajax({
        url: hostUrl + '/baby-seats',
        method: "GET",
        success: function (babySeats) {
            var filterBabySeats = babySeats.filter(function (babySeat) {
                return (babySeat.pickupPointDTO.id == pickupPoint_id);
            });
            $("#filterEquipment").find("#babySeat").html($.tmplLoader('selectBabySeat', {babySeats: filterBabySeats}));
            callback();
        }
    });
}

function sendOrderToEmails(order) {
    $.ajax({
        beforeSend: function (xhr) {
            if (localStorage.length !== 0) {
                var user = JSON.parse(localStorage.getItem(localStorage.key(0)));
                xhr.setRequestHeader('Authorization', user.authHeader);
            }
        },
        url: hostUrl + '/orders/sendEmail',
        contentType: "application/json",
        method: "POST",
        data: JSON.stringify(order),
        success: function (order) {
            console.log("Data: " + order);
        },
        error: function (error) {
            console.log(status);
        },
        dataType: "json"
    });
}


