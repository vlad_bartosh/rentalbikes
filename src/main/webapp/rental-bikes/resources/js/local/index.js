$.tmplLoader.ready(function () {
    $(document).ready(function () {
        if (localStorage.length !== 0) {
            var user = JSON.parse(localStorage.getItem(localStorage.key(0)));
            $('#mainPage').attr('href', 'index.html');
            $.get(hostUrl + "/rental-bikes/templates/right-logout.tmpl.html", function (data) {
                $("#right-bar").html(data);
            });
            $.ajax({
                url: hostUrl + '/user/' + user.idUser,
                contentType: "application/json",
                method: "GET",
                success: function (user) {
                    $('#userHello').text("Hello, " + user.login);
                    $('#right-bar').find('#logoutButton').click(function () {
                        localStorage.removeItem(localStorage.key(0));
                        window.location.href = '/rental-bikes/index.html';
                    })
                },
                error: function (error) {
                    if (error.status == 404) {
                        localStorage.removeItem(localStorage.key(0));
                        window.location.href = '/rental-bikes/login.html';
                    }

                    console.log(error.responseJSON.message);
                }
            });
        } else {
            $('#userHello').text("Hello,visitor! Welcome to RentalBikes.");
            $.get(hostUrl + "/rental-bikes/templates/right-sign-up.tmpl.html", function (data) {
                $("#right-bar").html(data);
            });
        }
    });
});
