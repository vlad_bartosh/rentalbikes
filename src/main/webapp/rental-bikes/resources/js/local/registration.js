$(document).ready(function () {
    $("#register").click(function (event) {
        var login = $('#login').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var passwordCheck = $('#password-confirm').val();
        var telephone = $('#telephone').val();
        $('#error-password-check').text("");
        $('#fillFields-error').text("");
        if (login === "" || email === "" || password === "" || passwordCheck === "") {
            $('#fillFields-error').text("Please, fill all fields");
            return;
        }
        if (password !== passwordCheck) {
            var errorMsg = " It is required that the passwords match";
            $('#error-password-check').text(errorMsg);
            return;
        }
        if (!validateEmail(email)) {
            $('#fillFields-error').text("Incorrect email");
            return;
        }
        if (!validateTelephone(telephone)) {
            $('#fillFields-error').text("Incorrect telephone");
            return;
        }
        if (password.length < 6) {
            $('#fillFields-error').text("Password too short");
            return;
        }
        var user = {
            "id": null,
            "login": login,
            "email": email,
            "telephone": telephone,
            "password": passwordCheck,
            "roles": null
        };
        $.ajax({
            url: hostUrl + '/user/sign-up',
            contentType: "application/json",
            method: "POST",
            data: JSON.stringify(user),
            success: function (user) {
                var authToken = make_base_auth(login, passwordCheck);
                var localStorageObject = {
                    "idUser": user.id,
                    "authHeader": authToken
                };
                console.log("Data: " + user.login + "\nStatus: " + status);
                localStorage.setItem("rentalBikes", JSON.stringify(localStorageObject));
                window.location.href = '/rental-bikes/index.html?idUser=' + user.id;
            },
            error: function (error) {
                var error_msg = error.responseJSON.message;
                $('#fillFields-error').text(error_msg);
            }
        });
    });
});

function make_base_auth(user, password) {
    var tok = user + ':' + password;
    var hash = window.btoa(tok);
    return "Basic " + hash;
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
}

function validateTelephone(tel) {
    var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
    return re.test(tel.toLowerCase());
}

