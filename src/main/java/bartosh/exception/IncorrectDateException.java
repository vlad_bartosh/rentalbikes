package bartosh.exception;

public class IncorrectDateException extends RuntimeException {
    public IncorrectDateException() {
    }

    public IncorrectDateException(String message) {
        super(message);
    }

    public IncorrectDateException(String message, Throwable cause) {
        super(message, cause);
    }

    public IncorrectDateException(Throwable cause) {
        super(cause);
    }
}
