package bartosh.exception;

public class EntitytNotFoundException extends RuntimeException {
    public EntitytNotFoundException() {
    }

    public EntitytNotFoundException(String message) {
        super(message);
    }

    public EntitytNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntitytNotFoundException(Throwable cause) {
        super(cause);
    }
}
