package bartosh.exception;

import bartosh.controller.dto.ApiError;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class UserControllerHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({RegistrationParameterIsExistException.class})
    public ResponseEntity<Object> handleEmailExist(RegistrationParameterIsExistException ex, WebRequest request) {
        String error = ex.getMessage();
        ApiError apiError =
                new ApiError(HttpStatus.CONFLICT, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({IncorrectPasswordException.class})
    public ResponseEntity<Object> handlePasswordIncorrect(IncorrectPasswordException ex, WebRequest request) {
        String error = ex.getMessage();
        ApiError apiError =
                new ApiError(HttpStatus.UNPROCESSABLE_ENTITY, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({UserDoesNotExistException.class})
    public ResponseEntity<Object> handleUserIsExist(UserDoesNotExistException ex, WebRequest request) {
        String error = ex.getMessage();

        ApiError apiError =
                new ApiError(HttpStatus.UNPROCESSABLE_ENTITY, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({EntitytNotFoundException.class})
    public ResponseEntity<Object> handleUserIsExist(EntitytNotFoundException ex, WebRequest request) {
        String error = ex.getMessage();

        ApiError apiError =
                new ApiError(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), error);
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

}
