package bartosh.config;

import bartosh.service.mapperConfig.CrUpdDTOtoOrder;
import bartosh.service.mapperConfig.DTOtoOrderMapper;
import bartosh.service.mapperConfig.OrderDTOMapper;
import bartosh.service.mapperConfig.OrderToEntityMapper;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "bartosh.controller")
public class BeanConfig {

    @Bean
    public ModelMapper modelMapper() {
        ModelMapper mapper = new ModelMapper();
        mapper.addMappings(new OrderDTOMapper());
        mapper.addMappings(new DTOtoOrderMapper());
        mapper.addMappings(new CrUpdDTOtoOrder());
        mapper.addMappings(new OrderToEntityMapper());
        return mapper;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
