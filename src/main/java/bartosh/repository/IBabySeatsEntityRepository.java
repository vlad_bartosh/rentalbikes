package bartosh.repository;

import bartosh.repository.ICRUDRepository;
import bartosh.repository.entity.criteria.EquipmentEntityCriteria;
import bartosh.repository.entity.equipment.BabySeatEntity;

import java.util.List;

public interface IBabySeatsEntityRepository extends ICRUDRepository<BabySeatEntity> {
    List<BabySeatEntity> getAll(EquipmentEntityCriteria equipmentEntityCriteria);
}
