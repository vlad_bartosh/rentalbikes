package bartosh.repository;

import bartosh.repository.entity.ProducerEntity;

import java.util.List;

public interface IProducerRepository extends ICRUDRepository<ProducerEntity> {
    List<ProducerEntity> getAll(String name);
}
