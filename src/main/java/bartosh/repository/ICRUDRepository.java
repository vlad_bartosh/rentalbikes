package bartosh.repository;

import java.util.List;

public interface ICRUDRepository<T> {
    List<T> getAll();

    T getById(Integer id);

    T insert(T t);

    T update(T t);

    T deleteById(Integer id);
}
