package bartosh.repository;

import bartosh.repository.ICRUDRepository;
import bartosh.repository.entity.criteria.EquipmentEntityCriteria;
import bartosh.repository.entity.equipment.HelmetEntity;

import java.util.List;

public interface IHelmetEntityRepository extends ICRUDRepository<HelmetEntity> {
    List<HelmetEntity> getAll(EquipmentEntityCriteria equipmentEntityCriteria);
}
