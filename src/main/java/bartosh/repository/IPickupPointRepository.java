package bartosh.repository;

import bartosh.repository.entity.PickupPointEntity;

import java.util.List;

public interface IPickupPointRepository extends ICRUDRepository<PickupPointEntity> {
    List<PickupPointEntity> getAll(String name, String address);
}
