package bartosh.repository;

import bartosh.repository.ICRUDRepository;
import bartosh.repository.entity.criteria.CostEntityCriteria;
import bartosh.repository.entity.CostEntity;

import java.util.List;

public interface ICostEntityRepository extends ICRUDRepository<CostEntity> {
    List<CostEntity> getAll(CostEntityCriteria costEntityCriteria);
}
