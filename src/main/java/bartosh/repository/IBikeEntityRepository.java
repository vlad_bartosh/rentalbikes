package bartosh.repository;

import bartosh.repository.entity.BikeEntity;
import bartosh.repository.entity.criteria.BikeEntityCriteria;

import java.util.List;

public interface IBikeEntityRepository extends ICRUDRepository<BikeEntity> {
    List<BikeEntity> getAll(BikeEntityCriteria bikeEntityCriteria);

    Long getBikeEntityTotalRows(BikeEntityCriteria bikeEntityCriteria);
}
