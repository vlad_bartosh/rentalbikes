package bartosh.repository;

import bartosh.repository.entity.UserEntity;

public interface IUserEntityRepository extends ICRUDRepository<UserEntity> {
    UserEntity getUserByCriteria(String login, String email);

    UserEntity findByLogin(String login);

    Boolean emailIsExist(String email);
}
