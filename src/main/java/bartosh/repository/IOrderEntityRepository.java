package bartosh.repository;

import bartosh.repository.entity.OrderEntity;

import java.util.List;

public interface IOrderEntityRepository extends ICRUDRepository<OrderEntity> {
    List<OrderEntity> getAll(Integer user_id);
}
