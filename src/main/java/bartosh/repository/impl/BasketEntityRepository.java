package bartosh.repository.impl;

import bartosh.repository.IBasketEntityRepository;
import bartosh.repository.entity.CostEntity;
import bartosh.repository.entity.criteria.EquipmentEntityCriteria;
import bartosh.repository.entity.equipment.BasketEntity;
import bartosh.repository.entity.equipment.HelmetEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class BasketEntityRepository extends CRUDRepository<BasketEntity> implements IBasketEntityRepository {

    @Autowired
    public BasketEntityRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<BasketEntity> getAll(EquipmentEntityCriteria equipmentEntityCriteria) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<BasketEntity> criteria = builder.createQuery(BasketEntity.class);
        Root<BasketEntity> root = criteria.from(BasketEntity.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        if (equipmentEntityCriteria.getName() != null) {
            predicates.add(builder.equal(root.get("name"), equipmentEntityCriteria.getName()));
        }
        if (equipmentEntityCriteria.getProducer() != null) {
            predicates.add(builder.equal(root.get("producer"), equipmentEntityCriteria.getProducer()));
        }
        if (equipmentEntityCriteria.getCount() != null) {
            predicates.add(builder.equal(root.get("count"), equipmentEntityCriteria.getCount()));
        }
        Join<HelmetEntity, CostEntity> costEntityRoot = root.join("cost");
        if (equipmentEntityCriteria.getMinCost() != null) {
            predicates.add(builder.ge(costEntityRoot.get("cost"), equipmentEntityCriteria.getMinCost()));
        }
        if (equipmentEntityCriteria.getMaxCost() != null) {
            predicates.add(builder.le(costEntityRoot.get("cost"), equipmentEntityCriteria.getMaxCost()));
        }
        criteria.where(predicates.toArray(new Predicate[]{}));
        return session.createQuery(criteria).getResultList();
    }
}
