package bartosh.repository.impl;

import bartosh.repository.IUserEntityRepository;
import bartosh.repository.entity.UserEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class UserEntityRepository extends CRUDRepository<UserEntity> implements IUserEntityRepository {
    @Autowired
    public UserEntityRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public UserEntity getUserByCriteria(String login, String email) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<UserEntity> criteria = builder.createQuery(UserEntity.class);
        Root<UserEntity> root = criteria.from(UserEntity.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        if (login != null && !login.equals("")) {
            predicates.add(builder.equal(root.get("login"), login));
        }
        if (email != null && !email.equals("")) {
            predicates.add(builder.equal(root.get("email"), email));
        }
        criteria.where(predicates.toArray(new Predicate[]{}));
        List<UserEntity> userEntity = session.createQuery(criteria).getResultList();
        return userEntity.size() != 0 ? userEntity.get(0) : null;

    }

    @Override
    public UserEntity findByLogin(String login) {
        List<UserEntity> userEntity = sessionFactory.getCurrentSession().createQuery("from UserEntity where login=:login").
                setParameter("login", login).getResultList();
        return userEntity.size() != 0 ? userEntity.get(0) : null;
    }

    public Boolean emailIsExist(String email) {
        return sessionFactory.getCurrentSession().createQuery("from UserEntity where email=:email").
                setParameter("email", email).getSingleResult() != null;
    }

}
