package bartosh.repository.impl;

import bartosh.repository.IProducerRepository;
import bartosh.repository.entity.ProducerEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class ProducerEntityRepository extends CRUDRepository<ProducerEntity> implements IProducerRepository {
    @Autowired
    public ProducerEntityRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<ProducerEntity> getAll(String name) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<ProducerEntity> criteria = builder.createQuery(ProducerEntity.class);
        Root<ProducerEntity> root = criteria.from(ProducerEntity.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        if (name != null && name.equals("")) {
            predicates.add(builder.equal(root.get("name"), name));
        }
        criteria.where(predicates.toArray(new Predicate[]{}));
        return session.createQuery(criteria).getResultList();
    }
}
