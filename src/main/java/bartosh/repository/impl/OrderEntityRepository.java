package bartosh.repository.impl;

import bartosh.repository.IOrderEntityRepository;
import bartosh.repository.entity.OrderEntity;
import bartosh.repository.entity.UserEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.*;

import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class OrderEntityRepository extends CRUDRepository<OrderEntity> implements IOrderEntityRepository {

    final SessionFactory sessionFactory;

    @Autowired
    public OrderEntityRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<OrderEntity> getAll(Integer user_id) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<OrderEntity> criteria = builder.createQuery(OrderEntity.class);
        Root<OrderEntity> root = criteria.from(OrderEntity.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        if (user_id != null) {
            Join<OrderEntity, UserEntity> userEntityRoot = root.join("user");
            predicates.add(builder.equal(userEntityRoot.get("id"), user_id));
        }
        criteria.where(predicates.toArray(new Predicate[]{}));
        return sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
    }

}
