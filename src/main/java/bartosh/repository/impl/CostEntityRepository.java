package bartosh.repository.impl;

import bartosh.repository.ICostEntityRepository;
import bartosh.repository.entity.CostEntity;
import bartosh.repository.entity.criteria.CostEntityCriteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class CostEntityRepository extends CRUDRepository<CostEntity> implements ICostEntityRepository {

    public CostEntityRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<CostEntity> getAll(CostEntityCriteria costEntityCriteria) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<CostEntity> criteria = builder.createQuery(CostEntity.class);
        Root<CostEntity> root = criteria.from(CostEntity.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        if (costEntityCriteria.getTypeCost() != null) {
            predicates.add(builder.equal(root.get("typeCost"), costEntityCriteria.getTypeCost()));
        }
        if (costEntityCriteria.getMinCost() != null) {
            predicates.add(builder.ge(root.get("cost"), costEntityCriteria.getMinCost()));
        }
        if (costEntityCriteria.getMaxCost() != null) {
            predicates.add(builder.le(root.get("cost"), costEntityCriteria.getMaxCost()));
        }
        criteria.where(predicates.toArray(new Predicate[]{}));
        return sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
    }
}
