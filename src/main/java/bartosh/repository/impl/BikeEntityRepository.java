package bartosh.repository.impl;

import bartosh.repository.IBikeEntityRepository;
import bartosh.repository.entity.*;
import bartosh.repository.entity.criteria.BikeEntityCriteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
@Transactional
public class BikeEntityRepository extends CRUDRepository<BikeEntity> implements IBikeEntityRepository {

    @Autowired
    public BikeEntityRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<BikeEntity> getAll(BikeEntityCriteria bikeEntityCriteria) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<BikeEntity> criteria = builder.createQuery(BikeEntity.class);
        Root<BikeEntity> root = criteria.from(BikeEntity.class);
        List<Predicate> predicates = new ArrayList<>();
        Join<BikeEntity, ProducerEntity> producerEntityRoot = root.join("producer");
        predicates.add(builder.equal(root.get("status"), Status.AVAILABLE_TO_RENT));//TODO[rfisenko 12/18/17]: set this filter on service layer
        if (bikeEntityCriteria.getProducer() != null && !Objects.equals(bikeEntityCriteria.getProducer(), "")) {
            predicates.add(builder.equal(producerEntityRoot.get("name"), bikeEntityCriteria.getProducer()));
        }
        if (bikeEntityCriteria.getTypeBike() != null) {
            predicates.add(builder.equal(root.get("typeBike"), bikeEntityCriteria.getTypeBike()));
        }
        if (bikeEntityCriteria.getColor() != null) {
            predicates.add(builder.equal(root.get("color"), bikeEntityCriteria.getColor()));
        }
        if (bikeEntityCriteria.getStatus() != null) {
            predicates.add(builder.equal(root.get("status"), bikeEntityCriteria.getStatus()));
        }
        if (bikeEntityCriteria.getSex() != null) {
            predicates.add(builder.equal(root.get("sex"), bikeEntityCriteria.getSex()));
        }
        if (bikeEntityCriteria.getAgeType() != null) {
            predicates.add(builder.equal(root.get("ageType"), bikeEntityCriteria.getAgeType()));
        }
        Join<BikeEntity, PickupPointEntity> pickupPointEntityRoot = root.join("pickupPointEntity");
        if (bikeEntityCriteria.getPickupPoint() != null && !Objects.equals(bikeEntityCriteria.getPickupPoint(), "")) {
            predicates.add(builder.equal(pickupPointEntityRoot.get("name"), bikeEntityCriteria.getPickupPoint()));
        }
        Join<BikeEntity, CostEntity> costEntityRoot = root.join("costEntity");
        if (bikeEntityCriteria.getMinCost() != null) {
            predicates.add(builder.ge(costEntityRoot.get("cost"), bikeEntityCriteria.getMinCost()));
        }
        if (bikeEntityCriteria.getMaxCost() != null) {
            predicates.add(builder.le(costEntityRoot.get("cost"), bikeEntityCriteria.getMaxCost()));
        }
        if (bikeEntityCriteria.getMinCost() != null && bikeEntityCriteria.getMaxCost() != null) {
            predicates.add(builder.between(costEntityRoot.get("cost"), bikeEntityCriteria.getMinCost(), bikeEntityCriteria.getMaxCost()));
        }
        criteria.where(predicates.toArray(new Predicate[]{}));
        if (bikeEntityCriteria.getOffset() == null) {
            bikeEntityCriteria.setOffset(0L);
        }
        if (bikeEntityCriteria.getLimit() == null) {
            bikeEntityCriteria.setLimit(getBikeEntityTotalRows(bikeEntityCriteria));
        }
        return sessionFactory.getCurrentSession().createQuery(criteria).setFirstResult(Math.toIntExact(bikeEntityCriteria.getOffset())).setMaxResults(Math.toIntExact(bikeEntityCriteria.getLimit())).getResultList();
    }

    public Long getBikeEntityTotalRows(BikeEntityCriteria bikeEntityCriteria) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = builder.createQuery(Long.class);
        Root<BikeEntity> root = criteriaQuery.from(BikeEntity.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        predicates.add(builder.equal(root.get("status"), Status.AVAILABLE_TO_RENT));
        if (bikeEntityCriteria.getProducer() != null && !Objects.equals(bikeEntityCriteria.getProducer(), "")) {
            Join<BikeEntity, ProducerEntity> producerEntityRoot = root.join("producer");
            predicates.add(builder.equal(producerEntityRoot.get("name"), bikeEntityCriteria.getProducer()));
        }
        if (bikeEntityCriteria.getTypeBike() != null) {
            predicates.add(builder.equal(root.get("typeBike"), bikeEntityCriteria.getTypeBike()));
        }
        if (bikeEntityCriteria.getColor() != null) {
            predicates.add(builder.equal(root.get("color"), bikeEntityCriteria.getColor()));
        }
        if (bikeEntityCriteria.getStatus() != null) {
            predicates.add(builder.equal(root.get("status"), bikeEntityCriteria.getStatus()));
        }
        if (bikeEntityCriteria.getSex() != null) {
            predicates.add(builder.equal(root.get("sex"), bikeEntityCriteria.getSex()));
        }
        if (bikeEntityCriteria.getAgeType() != null) {
            predicates.add(builder.equal(root.get("ageType"), bikeEntityCriteria.getAgeType()));
        }

        if (bikeEntityCriteria.getPickupPoint() != null && !Objects.equals(bikeEntityCriteria.getPickupPoint(), "")) {
            Join<BikeEntity, PickupPointEntity> pickupPointEntityRoot = root.join("pickupPointEntity");
            predicates.add(builder.equal(pickupPointEntityRoot.get("name"), bikeEntityCriteria.getPickupPoint()));
        }
        Join<BikeEntity, CostEntity> costEntityRoot = root.join("costEntity");
        if (bikeEntityCriteria.getMinCost() != null) {
            predicates.add(builder.ge(costEntityRoot.get("cost"), bikeEntityCriteria.getMinCost()));
        }
        if (bikeEntityCriteria.getMaxCost() != null) {
            predicates.add(builder.le(costEntityRoot.get("cost"), bikeEntityCriteria.getMaxCost()));
        }
        if (bikeEntityCriteria.getMinCost() != null && bikeEntityCriteria.getMaxCost() != null) {
            predicates.add(builder.between(costEntityRoot.get("cost"), bikeEntityCriteria.getMinCost(), bikeEntityCriteria.getMaxCost()));
        }
        criteriaQuery.select(builder.count(root));
        criteriaQuery.where(predicates.toArray(new Predicate[]{}));
        return session.createQuery(criteriaQuery).getSingleResult();
    }
}
