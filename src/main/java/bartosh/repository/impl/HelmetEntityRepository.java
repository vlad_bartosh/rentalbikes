package bartosh.repository.impl;

import bartosh.repository.IHelmetEntityRepository;
import bartosh.repository.entity.CostEntity;
import bartosh.repository.entity.criteria.EquipmentEntityCriteria;
import bartosh.repository.entity.equipment.HelmetEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class HelmetEntityRepository extends CRUDRepository<HelmetEntity> implements IHelmetEntityRepository {

    public HelmetEntityRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<HelmetEntity> getAll(EquipmentEntityCriteria equipmentEntityCriteria) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<HelmetEntity> criteria = builder.createQuery(HelmetEntity.class);
        Root<HelmetEntity> root = criteria.from(HelmetEntity.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        if (equipmentEntityCriteria.getName() != null) {
            predicates.add(builder.equal(root.get("name"), equipmentEntityCriteria.getName()));
        }
        if (equipmentEntityCriteria.getProducer() != null) {
            predicates.add(builder.equal(root.get("producer"), equipmentEntityCriteria.getProducer()));
        }
        if (equipmentEntityCriteria.getCount() != null) {
            predicates.add(builder.equal(root.get("count"), equipmentEntityCriteria.getCount()));
        }
        Join<HelmetEntity, CostEntity> costEntityRoot = root.join("cost");
        if (equipmentEntityCriteria.getMinCost() != null) {
            predicates.add(builder.ge(costEntityRoot.get("cost"), equipmentEntityCriteria.getMinCost()));
        }
        if (equipmentEntityCriteria.getMaxCost() != null) {
            predicates.add(builder.le(costEntityRoot.get("cost"), equipmentEntityCriteria.getMaxCost()));
        }
        criteria.where(predicates.toArray(new Predicate[]{}));
        return sessionFactory.getCurrentSession().createQuery(criteria).getResultList();
    }
}

