package bartosh.repository.impl;

import bartosh.repository.ICRUDRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.ParameterizedType;
import java.util.List;

@Repository
@Transactional
public abstract class CRUDRepository<T> implements ICRUDRepository<T> {

    private final static Logger LOGGER = LogManager.getLogger();
    protected SessionFactory sessionFactory;

    private final Class<T> persistentClass;

    @SuppressWarnings("unchecked")
    public CRUDRepository(SessionFactory sessionFactory) {
        this.persistentClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass
                ()).getActualTypeArguments()[0];
        this.sessionFactory = sessionFactory;
    }


    @Override
    @SuppressWarnings("unchecked")
    public List<T> getAll() {
        LOGGER.info("Get all from " + persistentClass.getName());
        return (List<T>) sessionFactory.getCurrentSession().createQuery("from " + persistentClass.getName()).getResultList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public T getById(Integer id) {
        LOGGER.info("GetById from " + persistentClass.getName() + " where id: " + id);
        return (T) sessionFactory.getCurrentSession().get(persistentClass.getName(), id);
    }

    @Override
    public T insert(T object) {
        sessionFactory.getCurrentSession().save(object);
        return object;
    }

    @Override
    public T update(T object) {
        sessionFactory.getCurrentSession().update(object);
        return object;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T deleteById(Integer id) {
        T dltObject = (T) sessionFactory.getCurrentSession().get(persistentClass.getName(), id);
        sessionFactory.getCurrentSession().delete(dltObject);
        return dltObject;
    }
}
