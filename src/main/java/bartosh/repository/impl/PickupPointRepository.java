package bartosh.repository.impl;

import bartosh.repository.IPickupPointRepository;
import bartosh.repository.entity.PickupPointEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class PickupPointRepository extends CRUDRepository<PickupPointEntity> implements IPickupPointRepository {
    public PickupPointRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<PickupPointEntity> getAll(String name, String address) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<PickupPointEntity> criteria = builder.createQuery(PickupPointEntity.class);
        Root<PickupPointEntity> root = criteria.from(PickupPointEntity.class);
        List<Predicate> predicates = new ArrayList<Predicate>();
        if (name != null) {
            predicates.add(builder.equal(root.get("name"), name));
        }
        if (address != null) {
            predicates.add(builder.equal(root.get("address"), address));
        }
        criteria.where(predicates.toArray(new Predicate[]{}));
        return session.createQuery(criteria).getResultList();
    }

}
