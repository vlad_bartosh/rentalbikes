package bartosh.repository;

import bartosh.repository.ICRUDRepository;
import bartosh.repository.entity.criteria.EquipmentEntityCriteria;
import bartosh.repository.entity.equipment.BasketEntity;

import java.util.List;

public interface IBasketEntityRepository extends ICRUDRepository<BasketEntity> {
    List<BasketEntity> getAll(EquipmentEntityCriteria equipmentEntityCriteria);
}
