package bartosh.repository.entity;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Builder
@Table(name = "bikes")
public class BikeEntity extends BaseEntity implements Serializable {
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "producer_id")
    private ProducerEntity producer;
    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    @NotNull
    private TypeBike typeBike;
    @Enumerated(EnumType.STRING)
    @Column(name = "color")
    private Color color;
    @Column(name = "status")
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status;
    @Column(name = "sex")
    @Enumerated(EnumType.STRING)
    @NotNull
    private Sex sex;
    @Column(name = "ageType")
    @Enumerated(EnumType.STRING)
    @NotNull
    private AgeType ageType;
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "cost_id")
    private CostEntity costEntity;
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "pickupPoint_id")
    private PickupPointEntity pickupPointEntity;
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "bikes", cascade = CascadeType.ALL)
    private List<OrderEntity> orderEntity;

    public String toString() {
        return "BikeEntity(producer=" + this.getProducer() + ", typeBike=" + this.getTypeBike() + ", color=" + this.getColor() + ", status=" + this.getStatus() + ", sex=" + this.getSex() + ", ageType=" + this.getAgeType() + ", costEntity=" + this.getCostEntity() + ", pickupPointEntity=" + this.getPickupPointEntity() + ")";
    }
}
