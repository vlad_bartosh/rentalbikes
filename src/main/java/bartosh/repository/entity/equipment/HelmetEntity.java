package bartosh.repository.entity.equipment;

import bartosh.repository.entity.AgeType;
import bartosh.repository.entity.Color;
import bartosh.repository.entity.CostEntity;
import bartosh.repository.entity.PickupPointEntity;
import bartosh.repository.entity.ProducerEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@Entity
@DiscriminatorValue("helmet")
public class HelmetEntity extends EquipmentEntity {
    @Enumerated(EnumType.STRING)
    @Column(name = "ageType")
    private AgeType ageType;
    @Enumerated(EnumType.STRING)
    @Column(name = "color")
    private Color color;

    public HelmetEntity(String name, ProducerEntity producer, CostEntity costEntity, Integer count, PickupPointEntity pickupPointEntity, AgeType ageType, Color color) {
        super(name, producer, costEntity, count, pickupPointEntity);
        this.ageType = ageType;
        this.color = color;
    }
}
