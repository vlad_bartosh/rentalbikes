package bartosh.repository.entity.equipment;

import bartosh.repository.entity.BaseEntity;
import bartosh.repository.entity.CostEntity;
import bartosh.repository.entity.PickupPointEntity;
import bartosh.repository.entity.ProducerEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DiscriminatorOptions;

import javax.persistence.*;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "equipment")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "typeEquipment", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue(value = "equipment")
@DiscriminatorOptions(force = true)
public abstract class EquipmentEntity extends BaseEntity implements Serializable {
    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "producer_id")
    private ProducerEntity producer;

    @ManyToOne
    @JoinColumn(name = "cost_id")
    private CostEntity cost;

    @Column(name = "count")
    private Integer count;

    @ManyToOne
    @JoinColumn(name = "pickupPoint_id")
    private PickupPointEntity pickupPointEntity;

}
