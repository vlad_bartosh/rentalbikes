package bartosh.repository.entity.equipment;


import bartosh.repository.entity.CostEntity;
import bartosh.repository.entity.PickupPointEntity;
import bartosh.repository.entity.ProducerEntity;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@Entity
@DiscriminatorValue("babySeat")
public class BabySeatEntity extends EquipmentEntity {
    public BabySeatEntity(String name, ProducerEntity producer, CostEntity costEntity, Integer count, PickupPointEntity pickupPointEntity) {
        super(name, producer, costEntity, count, pickupPointEntity);
    }
}
