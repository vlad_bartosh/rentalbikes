package bartosh.repository.entity.equipment;

import bartosh.repository.entity.CostEntity;
import bartosh.repository.entity.PickupPointEntity;
import bartosh.repository.entity.ProducerEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@Entity
@DiscriminatorValue("basket")
public class BasketEntity extends EquipmentEntity {
    public BasketEntity(String name, ProducerEntity producer, CostEntity costEntity, Integer count, PickupPointEntity pickupPointEntity) {
        super(name, producer, costEntity, count, pickupPointEntity);
    }
}
