package bartosh.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "producer")
public class ProducerEntity extends BaseEntity implements Serializable {
    @Column(name = "name")
    @NotNull
    private String name;
    @Column(name = "address")
    @NotNull
    private String address;
}
