package bartosh.repository.entity;

public enum Color {
    YELLOW, BLACK, WHITE, BLUE, RED, ORANGE, PINK, BROWN, GREEN
}
