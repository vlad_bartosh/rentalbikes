package bartosh.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "pickupPoint")
public class PickupPointEntity extends BaseEntity implements Serializable {
    @Column(name = "name")
    @NotNull
    private String name;
    @Column(name = "address")
    @NotNull
    private String address;
    @Column(name = "latitude")
    @NotNull
    private String latitude;
    @Column(name = "longtitude")
    @NotNull
    private String longtitude;
}
