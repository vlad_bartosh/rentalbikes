package bartosh.repository.entity;

public enum AgeType {
    CHILD, TEENAGER, ADULT
}
