package bartosh.repository.entity.criteria;

import bartosh.repository.entity.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class BikeEntityCriteria {
    private String producer;
    private TypeBike typeBike;
    private Color color;
    private Status status;
    private Sex sex;
    private AgeType ageType;
    private String pickupPoint;
    private Date rentFrom;
    private Date rentTill;
    private Integer minCost;
    private Integer maxCost;
    private Long offset;
    private Long limit;
}

