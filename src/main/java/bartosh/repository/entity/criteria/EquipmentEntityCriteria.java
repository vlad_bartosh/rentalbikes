package bartosh.repository.entity.criteria;

import bartosh.repository.entity.AgeType;
import bartosh.repository.entity.Color;
import bartosh.repository.entity.ProducerEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EquipmentEntityCriteria {
    String name;
    ProducerEntity producer;
    AgeType ageType;
    Color color;
    Integer minCost;
    Integer maxCost;
    Integer count;
}
