package bartosh.repository.entity.criteria;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CostEntityCriteria {
    private String typeCost;
    private Integer minCost;
    private Integer maxCost;
}
