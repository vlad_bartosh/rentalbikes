package bartosh.repository.entity;

public enum Role {
    ROLE_ADMIN, ROLE_USER, ROLE_WORKER, ROLE_VISITOR
}
