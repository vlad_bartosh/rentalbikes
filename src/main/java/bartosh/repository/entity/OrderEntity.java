package bartosh.repository.entity;

import bartosh.repository.entity.equipment.EquipmentEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user_order")
public class OrderEntity extends BaseEntity implements Serializable {
    @NotNull
    @ManyToOne(targetEntity = PickupPointEntity.class, cascade = CascadeType.MERGE)
    @JoinColumn(name = "pickup_id")
    private PickupPointEntity pickupPoint;
    @NotNull
    @ManyToOne(targetEntity = PickupPointEntity.class, cascade = CascadeType.MERGE)
    @JoinColumn(name = "pickoff_id")
    private PickupPointEntity pickoffPoint;
    @NotNull
    @Column(name = "rentFrom", columnDefinition = "DATETIME")
    private Date rentFrom;
    @NotNull
    @Column(name = "rentTill", columnDefinition = "DATETIME")
    private Date rentTill;
    @ManyToMany(fetch = FetchType.LAZY, targetEntity = BikeEntity.class, cascade = CascadeType.ALL)
    @JoinTable(name = "orders_bikes",
            joinColumns = @JoinColumn(name = "order_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "bike_id", referencedColumnName = "id"))
    private List<BikeEntity> bikes;
    @ManyToMany(targetEntity = EquipmentEntity.class, cascade = CascadeType.ALL)
    @JoinTable(name = "orders_equipment",
            joinColumns = @JoinColumn(name = "order_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "equipment_id", referencedColumnName = "id"))
    private List<EquipmentEntity> equipment;
    @NotNull
    @OneToOne(targetEntity = UserEntity.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private UserEntity user;
    @NotNull
    @Column(name = "finalCost")
    private BigDecimal finalCost;
    @NotNull
    @Column(name = "discount")
    private Integer discount;

    public String toString() {
        return "OrderEntity(pickupPoint=" + this.getPickupPoint() + ", pickoffPoint=" + this.getPickoffPoint() + ", rentFrom=" + this.getRentFrom() + ", rentTill=" + this.getRentTill() + ", equipment=" + this.getEquipment() + ", user=" + this.getUser() + ", finalCost=" + this.getFinalCost() + ", discount=" + this.getDiscount() + ")";
    }
}
