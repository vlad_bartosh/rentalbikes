package bartosh.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "cost")
public class CostEntity extends BaseEntity implements Serializable {
    @NotNull
    @Column(name = "type_cost")
    private String typeCost;
    @NotNull
    @Column(name = "cost")
    private Integer cost;


}
