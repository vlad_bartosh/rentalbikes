package bartosh.repository.entity;

public enum TypeBike {
    ROAD, TOURING, HYBRID, CITY, MOUNTAIN
}
