package bartosh.repository.entity;

public enum Status {
    AVAILABLE_TO_RENT, RENTED, UNAVAILABLE_TO_RENT
}
