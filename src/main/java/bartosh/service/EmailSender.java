package bartosh.service;

import bartosh.service.model.Order;

public interface EmailSender {
    void sendToEmployee(Order order);
}
