package bartosh.service.mapperConfig;

import bartosh.controller.dto.OrderCreateUpdateDTO;
import bartosh.service.model.Order;
import org.modelmapper.PropertyMap;

public class CrUpdDTOtoOrder extends PropertyMap<OrderCreateUpdateDTO, Order> {
    @Override
    protected void configure() {
        skip().setEquipments(null);
    }
}
