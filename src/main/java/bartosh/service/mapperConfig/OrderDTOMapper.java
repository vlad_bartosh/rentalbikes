package bartosh.service.mapperConfig;

import bartosh.controller.dto.OrderDTO;
import bartosh.controller.dto.PickupPointDTO;
import bartosh.controller.dto.UserDTO;
import bartosh.service.model.Order;
import bartosh.service.model.PickupPoint;
import bartosh.service.model.User;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

public class OrderDTOMapper extends PropertyMap<Order, OrderDTO> {
    @Override
    protected void configure() {
        Converter<User, UserDTO> userToUserDTO = new AbstractConverter<User, UserDTO>() {
            protected UserDTO convert(User user) {
                UserDTO userDTO = new UserDTO();
                userDTO.setId(user.getId());
                userDTO.setLogin(user.getLogin());
                userDTO.setPassword(user.getPassword());
                userDTO.setEmail(user.getEmail());
                userDTO.setRoles(user.getRoles());
                return userDTO;
            }
        };
        Converter<PickupPoint, PickupPointDTO> pickupPointToDTO = new AbstractConverter<PickupPoint, PickupPointDTO>() {
            @Override
            protected PickupPointDTO convert(PickupPoint source) {
                PickupPointDTO pickupPointDTO = new PickupPointDTO();
                pickupPointDTO.setId(source.getId());
                pickupPointDTO.setName(source.getName());
                pickupPointDTO.setAddress(source.getAddress());
                pickupPointDTO.setLatitude(source.getLatitude());
                pickupPointDTO.setLongtitude(source.getLongtitude());
                return pickupPointDTO;
            }
        };
        skip().setEquipments(null);
    }
}
