package bartosh.service.mapperConfig;

import bartosh.controller.dto.OrderDTO;
import bartosh.controller.dto.PickupPointDTO;
import bartosh.controller.dto.equipment.BabySeatDTO;
import bartosh.controller.dto.equipment.BasketDTO;
import bartosh.controller.dto.equipment.EquipmentDTO;
import bartosh.controller.dto.equipment.HelmetDTO;
import bartosh.repository.entity.BaseEntity;
import bartosh.repository.entity.OrderEntity;
import bartosh.repository.entity.PickupPointEntity;
import bartosh.repository.entity.equipment.BabySeatEntity;
import bartosh.repository.entity.equipment.BasketEntity;
import bartosh.repository.entity.equipment.EquipmentEntity;
import bartosh.repository.entity.equipment.HelmetEntity;
import bartosh.service.model.BaseModel;
import bartosh.service.model.Order;
import bartosh.service.model.PickupPoint;
import bartosh.service.model.equipment.BabySeat;
import bartosh.service.model.equipment.Basket;
import bartosh.service.model.equipment.Equipment;
import bartosh.service.model.equipment.Helmet;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class EquipmentMapperHelper {

    private ModelMapper mapper;

    @Autowired
    public EquipmentMapperHelper(ModelMapper mapper) {
        this.mapper = mapper;
    }


    public List<Order> mapEquipmentEntityToEquipment(List<OrderEntity> orderEntities, List<Order> orders) {
        Map<Integer, List<Equipment>> equipmentListMap = orderEntities.stream().collect(Collectors.toMap(BaseEntity::getId,
                orderEntity -> orderEntity.getEquipment().stream().map(equipmentEntity -> {
                    if (equipmentEntity instanceof BasketEntity) {
                        return mapper.map(equipmentEntity, Basket.class);
                    }
                    if (equipmentEntity instanceof BabySeatEntity) {
                        return mapper.map(equipmentEntity, BabySeat.class);
                    }
                    if (equipmentEntity instanceof HelmetEntity) {
                        return mapper.map(equipmentEntity, Helmet.class);
                    }
                    return null;
                }).collect(Collectors.toList())));
        return orders.stream().peek(order -> order.setEquipments(equipmentListMap.get(order.getId()))).collect(Collectors.toList());
    }

    public List<OrderDTO> mapEquipmentToEquipmentDTO(List<Order> orders, List<OrderDTO> ordersDTO) {
        Map<Integer, List<EquipmentDTO>> equipmentListMap = orders.stream().collect(Collectors.toMap(BaseModel::getId,
                orderModel -> orderModel.getEquipments().stream().map(equipment -> {
                    if (equipment instanceof Basket) {
                        BasketDTO basketDTO = mapper.map(equipment, BasketDTO.class);
                        basketDTO.setPickupPointDTO(mapper.map(equipment.getPickupPoint(), PickupPointDTO.class));
                        return basketDTO;
                    }
                    if (equipment instanceof BabySeat) {
                        BabySeatDTO babySeatDTO = mapper.map(equipment, BabySeatDTO.class);
                        babySeatDTO.setPickupPointDTO(mapper.map(equipment.getPickupPoint(), PickupPointDTO.class));
                        return babySeatDTO;
                    }
                    if (equipment instanceof Helmet) {
                        HelmetDTO helmetDTO = mapper.map(equipment, HelmetDTO.class);
                        helmetDTO.setPickupPointDTO(mapper.map(equipment.getPickupPoint(), PickupPointDTO.class));
                        return helmetDTO;
                    }
                    return null;
                }).collect(Collectors.toList())));

        return ordersDTO.stream().peek(orderDTO -> orderDTO.setEquipments(equipmentListMap.get(orderDTO.getId()))).collect(Collectors.toList());
    }

    public List<HelmetDTO> helmetDTOPickupPointsMapping(List<Helmet> helmets, List<HelmetDTO> helmetsDTO) {
        Map<Integer, PickupPointDTO> helmetsMap = helmets.stream().collect(Collectors.toMap(
                bartosh.service.model.equipment.Helmet::getId, helmet -> mapper.map(helmet.getPickupPoint(), PickupPointDTO.class)));
        return helmetsDTO.stream().peek(helmetDTO -> helmetDTO.setPickupPointDTO(helmetsMap.get(helmetDTO.getId()))).collect(Collectors.toList());
    }

    public OrderEntity mapEquipmentToEquipmentEntity(Order order, OrderEntity orderEntity) {
        List<EquipmentEntity> equipmentListMap = order.getEquipments().stream().map(equipment -> {
            if (equipment instanceof Basket) {
                BasketEntity basketEntity = mapper.map(equipment, BasketEntity.class);
                basketEntity.setPickupPointEntity(mapper.map(equipment.getPickupPoint(), PickupPointEntity.class));
                return basketEntity;
            }
            if (equipment instanceof BabySeat) {
                BabySeatEntity babySeatEntity = mapper.map(equipment, BabySeatEntity.class);
                babySeatEntity.setPickupPointEntity(mapper.map(equipment.getPickupPoint(), PickupPointEntity.class));
                return babySeatEntity;
            }
            if (equipment instanceof Helmet) {
                HelmetEntity helmetEntity = mapper.map(equipment, HelmetEntity.class);
                helmetEntity.setPickupPointEntity(mapper.map(equipment.getPickupPoint(), PickupPointEntity.class));
                return helmetEntity;
            }
            return null;
        }).collect(Collectors.toList());
        orderEntity.setEquipment(equipmentListMap);
        return orderEntity;
    }

    public Order mapEquipmentEntityToEquipment(OrderEntity orderEntity, Order order) {
        List<Equipment> equipmentList = orderEntity.getEquipment().stream().map(equipmentEntity -> {
            if (equipmentEntity instanceof BasketEntity) {
                return mapper.map(equipmentEntity, Basket.class);
            }
            if (equipmentEntity instanceof BabySeatEntity) {
                return mapper.map(equipmentEntity, BabySeat.class);
            }
            if (equipmentEntity instanceof HelmetEntity) {
                return mapper.map(equipmentEntity, bartosh.service.model.equipment.Helmet.class);
            }
            return null;
        }).collect(Collectors.toList());
        order.setEquipments(equipmentList);
        return order;
    }

    public OrderDTO mapEquipmentToEquipmentDTO(Order order, OrderDTO orderDTO) {
        List<EquipmentDTO> equipmentList = order.getEquipments().stream().map(equipment -> {
            if (equipment instanceof Basket) {
                BasketDTO basketDTO = mapper.map(equipment, BasketDTO.class);
                basketDTO.setPickupPointDTO(mapper.map(equipment.getPickupPoint(), PickupPointDTO.class));
                return basketDTO;
            }
            if (equipment instanceof BabySeat) {
                BabySeatDTO babySeatDTO = mapper.map(equipment, BabySeatDTO.class);
                babySeatDTO.setPickupPointDTO(mapper.map(equipment.getPickupPoint(), PickupPointDTO.class));
                return babySeatDTO;
            }
            if (equipment instanceof bartosh.service.model.equipment.Helmet) {
                HelmetDTO helmetDTO = mapper.map(equipment, HelmetDTO.class);
                helmetDTO.setPickupPointDTO(mapper.map(equipment.getPickupPoint(), PickupPointDTO.class));
                return helmetDTO;
            }
            return null;
        }).collect(Collectors.toList());
        orderDTO.setEquipments(equipmentList);
        return orderDTO;
    }

    public Order mapEquipmentDTOToEquipment(OrderDTO orderDTO, Order order) {
        List<Equipment> equipmentList = orderDTO.getEquipments().stream().map(equipmentDTO -> {
            if (equipmentDTO instanceof BasketDTO) {
                Basket basket = mapper.map(equipmentDTO, Basket.class);
                basket.setPickupPoint(mapper.map(equipmentDTO.getPickupPointDTO(), PickupPoint.class));
                return basket;
            }
            if (equipmentDTO instanceof BabySeatDTO) {
                BabySeat babySeat = mapper.map(equipmentDTO, BabySeat.class);
                babySeat.setPickupPoint(mapper.map(equipmentDTO.getPickupPointDTO(), PickupPoint.class));
                return babySeat;
            }
            if (equipmentDTO instanceof HelmetDTO) {
                Helmet helmet = mapper.map(equipmentDTO, Helmet.class);
                helmet.setPickupPoint(mapper.map(equipmentDTO.getPickupPointDTO(), PickupPoint.class));
                return helmet;
            }
            return null;
        }).collect(Collectors.toList());
        order.setEquipments(equipmentList);
        return order;
    }
}
