package bartosh.service.mapperConfig;

import bartosh.repository.entity.OrderEntity;
import bartosh.service.model.Order;
import org.modelmapper.PropertyMap;


public class OrderToEntityMapper extends PropertyMap<Order, OrderEntity> {
    @Override
    protected void configure() {
        skip().setEquipment(null);
    }
}
