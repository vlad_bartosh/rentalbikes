package bartosh.service.mapperConfig;

import bartosh.repository.entity.OrderEntity;
import bartosh.repository.entity.UserEntity;
import bartosh.service.model.Order;
import bartosh.service.model.User;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;

public class EntityToOrderMapper extends PropertyMap<OrderEntity, Order> {
    @Override
    protected void configure() {
        Converter<UserEntity, User> orderEntityToOrder = new AbstractConverter<UserEntity, User>() {
            protected User convert(UserEntity userEntity) {
                User user = new User();
                user.setId(userEntity.getId());
                user.setLogin(userEntity.getLogin());
                user.setPassword(userEntity.getPassword());
                user.setEmail(userEntity.getEmail());
                user.setRoles(userEntity.getRoles());
                return user;
            }
        };
        using(orderEntityToOrder).map(source.getUser()).setUser(null);
    }
}
