package bartosh.service.mapperConfig;

import bartosh.controller.dto.OrderDTO;
import bartosh.service.model.Order;
import org.modelmapper.PropertyMap;


public class DTOtoOrderMapper extends PropertyMap<OrderDTO, Order> {
    @Override
    protected void configure() {
        skip().setEquipments(null);
    }
}
