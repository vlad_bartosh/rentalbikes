package bartosh.service.mapperConfig;

import bartosh.repository.entity.BikeEntity;
import bartosh.repository.entity.CostEntity;
import bartosh.repository.entity.PickupPointEntity;
import bartosh.service.model.Bike;
import bartosh.service.model.Cost;
import bartosh.service.model.PickupPoint;
import org.modelmapper.AbstractConverter;
import org.modelmapper.Converter;
import org.modelmapper.PropertyMap;
import org.modelmapper.spi.MappingContext;

public class BikeEntityMapper extends PropertyMap<Bike, BikeEntity> {


    @Override
    protected void configure() {
        Converter<Cost, CostEntity> costToCostEntity = new AbstractConverter<Cost, CostEntity>() {
            protected CostEntity convert(Cost cost) {
                CostEntity costEntity = new CostEntity();
                costEntity.setCost(cost.getCost());
                costEntity.setId(cost.getId());
                costEntity.setTypeCost(cost.getTypeCost());
                return costEntity;
            }
        };
        Converter<PickupPoint, PickupPointEntity> pickupPointToPickupPointEntity = new AbstractConverter<PickupPoint, PickupPointEntity>() {
            @Override
            protected PickupPointEntity convert(PickupPoint source) {
                PickupPointEntity result = new PickupPointEntity();
                if (source != null) {
                    result.setId(source.getId());
                    result.setAddress(source.getAddress());
                    result.setLatitude(source.getLatitude());
                    result.setLongtitude(source.getLongtitude());
                    result.setName(source.getName());

                }
                return result;
            }
        };
        using((MappingContext<Cost, CostEntity> context) -> {
            Cost cost = context.getSource();
            CostEntity costEntity = new CostEntity();
            costEntity.setCost(cost.getCost());
            costEntity.setId(cost.getId());
            costEntity.setTypeCost(cost.getTypeCost());
            return costEntity;
        }).map(source.getCost()).setCostEntity(null);
        using(pickupPointToPickupPointEntity).map(source.getPickupPoint()).setPickupPointEntity(null);
    }
}
