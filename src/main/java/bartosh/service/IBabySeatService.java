package bartosh.service;

import bartosh.service.model.criteria.EquipmentCriteria;
import bartosh.service.model.equipment.BabySeat;

import java.util.List;

public interface IBabySeatService extends IBaseService<BabySeat> {
    List<BabySeat> getAll(EquipmentCriteria equipmentCriteria);
}
