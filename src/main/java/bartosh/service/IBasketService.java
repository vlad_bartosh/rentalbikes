package bartosh.service;

import bartosh.service.model.criteria.EquipmentCriteria;
import bartosh.service.model.equipment.Basket;

import java.util.List;

public interface IBasketService extends IBaseService<Basket> {

    List<Basket> getAll(EquipmentCriteria equipmentCriteria);
}
