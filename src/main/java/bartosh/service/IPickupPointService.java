package bartosh.service;

import bartosh.service.model.PickupPoint;

import java.util.List;

public interface IPickupPointService extends IBaseService<PickupPoint> {

    List<PickupPoint> getAll(String name, String address);

}
