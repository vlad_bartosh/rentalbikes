package bartosh.service;

import bartosh.service.model.Producer;

public interface IProducerService extends IBaseService<Producer> {
}
