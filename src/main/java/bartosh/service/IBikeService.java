package bartosh.service;

import bartosh.service.model.Bike;
import bartosh.service.model.criteria.BikeCriteria;

import java.util.List;

public interface IBikeService extends IBaseService<Bike> {

    List<Bike> getAll(BikeCriteria bikeCriteria);

    Long getBikeEntityTotalRows(BikeCriteria bikeCriteria);
}
