package bartosh.service;

import bartosh.service.model.criteria.EquipmentCriteria;
import bartosh.service.model.equipment.Helmet;

import java.util.List;

public interface IHelmetService extends IBaseService<Helmet> {

    List<Helmet> getAll(EquipmentCriteria equipmentCriteria);
}
