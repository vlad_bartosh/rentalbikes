package bartosh.service;

import bartosh.service.model.Cost;
import bartosh.service.model.criteria.CostCriteria;

import java.util.List;

public interface ICostService extends IBaseService<Cost> {
    List<Cost> getAll(CostCriteria costCriteria);
}
