package bartosh.service;

import bartosh.service.model.BaseModel;

public interface IBaseService<T extends BaseModel> {
    T getById(Integer id);

    T add(T t);

    T update(T t);

    T delete(Integer id);
}
