package bartosh.service;

import bartosh.service.model.Order;

import java.math.BigDecimal;
import java.util.List;

public interface IOrderService extends IBaseService<Order> {
    List<Order> getAll(Integer user_id);

    Order add(Order order);

    Double getDiscount(Order order);

    BigDecimal calculateEquipmentCost(Order order);

    BigDecimal calculateBikesCost(Order order);

    BigDecimal calculateOrderCost(Integer hours, Double discount, BigDecimal bikesCost, BigDecimal equipmentCost);
}
