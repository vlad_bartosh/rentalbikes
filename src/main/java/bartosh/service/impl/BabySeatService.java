package bartosh.service.impl;

import bartosh.repository.IBabySeatsEntityRepository;
import bartosh.repository.entity.criteria.EquipmentEntityCriteria;
import bartosh.repository.entity.equipment.BabySeatEntity;
import bartosh.service.IBabySeatService;
import bartosh.service.model.criteria.EquipmentCriteria;
import bartosh.service.model.equipment.BabySeat;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BabySeatService extends BaseService<BabySeat, BabySeatEntity> implements IBabySeatService {

    private final ModelMapper mapper;
    private final IBabySeatsEntityRepository iBabySeatsEntityRepository;

    @Autowired
    public BabySeatService(ModelMapper mapper, IBabySeatsEntityRepository iBabySeatsEntityRepository) throws IllegalAccessException, InstantiationException {
        super(mapper, iBabySeatsEntityRepository);
        this.mapper = mapper;
        this.iBabySeatsEntityRepository = iBabySeatsEntityRepository;
    }

    @Override
    public List<BabySeat> getAll(EquipmentCriteria equipmentCriteria) {
        return mapper.map(iBabySeatsEntityRepository.getAll(mapper.map(equipmentCriteria, EquipmentEntityCriteria.class)), new TypeToken<List<BabySeat>>() {
        }.getType());
    }
}
