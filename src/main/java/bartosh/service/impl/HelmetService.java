package bartosh.service.impl;

import bartosh.repository.IHelmetEntityRepository;
import bartosh.repository.entity.criteria.EquipmentEntityCriteria;
import bartosh.repository.entity.equipment.HelmetEntity;
import bartosh.service.IHelmetService;
import bartosh.service.model.criteria.EquipmentCriteria;
import bartosh.service.model.equipment.Helmet;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class HelmetService extends BaseService<Helmet, HelmetEntity> implements IHelmetService {

    private final IHelmetEntityRepository iHelmetEntityRepository;
    private final ModelMapper mapper;

    @Autowired
    public HelmetService(ModelMapper modelMapper, IHelmetEntityRepository iHelmetEntityRepository) throws IllegalAccessException, InstantiationException {
        super(modelMapper, iHelmetEntityRepository);
        this.iHelmetEntityRepository = iHelmetEntityRepository;
        this.mapper = modelMapper;
    }

    @Override
    public List<Helmet> getAll(EquipmentCriteria equipmentCriteria) {
        return mapper.map(iHelmetEntityRepository.getAll(mapper.map(equipmentCriteria, EquipmentEntityCriteria.class)), new TypeToken<List<Helmet>>() {
        }.getType());
    }
}
