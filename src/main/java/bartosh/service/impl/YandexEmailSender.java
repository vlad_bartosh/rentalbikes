package bartosh.service.impl;

import bartosh.service.EmailSender;
import bartosh.service.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Arrays;
import java.util.Properties;

@PropertySource("classpath:mail.properties")
@Service
public class YandexEmailSender implements EmailSender {

    @Autowired
    private final Environment env;

    public YandexEmailSender(Environment env) {
        this.env = env;
    }

    @Override
    public void sendToEmployee(Order order) {
        env.getProperty("mail.smtp.host");
        Properties props = new Properties();
        props.put("mail.smtp.host", env.getProperty("mail.smtp.host"));
        props.put("mail.smtp.socketFactory.port", env.getProperty("mail.smtp.socketFactory.port"));
        props.put("mail.smtp.socketFactory.class",
                env.getProperty("mail.smtp.socketFactory.class"));
        props.put("mail.smtp.auth", env.getProperty("mail.smtp.auth"));
        props.put("mail.smtp.port", env.getProperty("mail.smtp.port"));

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(env.getProperty("companyEmail"), env.getProperty("emailPassword"));
                    }
                });


        String[] employeeEmails = env.getProperty("employeeEmail").split(",");
        Arrays.stream(employeeEmails).forEach(employeeEmail -> {
                    try {
                        Message message = new MimeMessage(session);
                        message.setFrom(new InternetAddress(env.getProperty("companyEmail")));
                        message.setRecipients(Message.RecipientType.TO,
                                InternetAddress.parse(employeeEmail));
                        message.setSubject("Order #" + order.getId());
                        String sendMessage = "User: " + order.getUser().getLogin() + "\n" +
                                "Telephone: " + order.getUser().getTelephone() + "\n" +
                                "PickupPoint: " + order.getPickupPoint().getName() + "\n" +
                                "PickoffPoint: " + order.getPickoffPoint().getName() + "\n" +
                                "Rent from: " + order.getRentFrom().toString() + "\n" +
                                "Rent till: " + order.getRentTill().toString() + "\n" +
                                "Final cost: " + order.getFinalCost().toString();
                        message.setText(sendMessage);
                        Transport.send(message);
                    } catch (MessagingException e) {
                        throw new RuntimeException(e);
                    }
                }
        );
    }

}
