package bartosh.service.impl;

import bartosh.repository.ICostEntityRepository;
import bartosh.repository.entity.criteria.CostEntityCriteria;
import bartosh.repository.entity.CostEntity;
import bartosh.service.ICostService;
import bartosh.service.model.Cost;
import bartosh.service.model.criteria.CostCriteria;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CostService extends BaseService<Cost, CostEntity> implements ICostService {

    private final ICostEntityRepository iCostEntityRepository;
    private final ModelMapper mapper;

    @Autowired
    public CostService(ModelMapper mapper, ICostEntityRepository iCostEntityRepository) throws IllegalAccessException, InstantiationException {
        super(mapper, iCostEntityRepository);
        this.iCostEntityRepository = iCostEntityRepository;
        this.mapper = mapper;
    }

    @Override
    public List<Cost> getAll(CostCriteria costCriteria) {
        return mapper.map(iCostEntityRepository.getAll(mapper.map(costCriteria, CostEntityCriteria.class)), new TypeToken<List<Cost>>() {
        }.getType());
    }
}
