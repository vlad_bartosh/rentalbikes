package bartosh.service.impl;

import bartosh.repository.IBasketEntityRepository;
import bartosh.repository.entity.criteria.EquipmentEntityCriteria;
import bartosh.repository.entity.equipment.BasketEntity;
import bartosh.service.IBasketService;
import bartosh.service.model.criteria.EquipmentCriteria;
import bartosh.service.model.equipment.Basket;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class BasketService extends BaseService<Basket, BasketEntity> implements IBasketService {

    private final ModelMapper mapper;
    private final IBasketEntityRepository iBasketEntityRepository;

    @Autowired
    public BasketService(ModelMapper mapper, IBasketEntityRepository iBasketEntityRepository) throws IllegalAccessException, InstantiationException {
        super(mapper, iBasketEntityRepository);
        this.mapper = mapper;
        this.iBasketEntityRepository = iBasketEntityRepository;
    }

    @Override
    public List<Basket> getAll(EquipmentCriteria equipmentCriteria) {
        return mapper.map(iBasketEntityRepository.getAll(mapper.map(equipmentCriteria, EquipmentEntityCriteria.class)), new TypeToken<List<Basket>>() {
        }.getType());
    }
}
