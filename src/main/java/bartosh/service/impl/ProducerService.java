package bartosh.service.impl;

import bartosh.repository.IProducerRepository;
import bartosh.repository.entity.ProducerEntity;
import bartosh.service.IProducerService;
import bartosh.service.model.Producer;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class ProducerService extends BaseService<Producer, ProducerEntity> implements IProducerService {


    @Autowired
    public ProducerService(ModelMapper mapper, IProducerRepository iProducerRepository) throws IllegalAccessException, InstantiationException {
        super(mapper, iProducerRepository);
    }
}
