package bartosh.service.impl;

import bartosh.repository.IOrderEntityRepository;
import bartosh.repository.entity.OrderEntity;
import bartosh.service.IOrderService;
import bartosh.service.mapperConfig.EquipmentMapperHelper;
import bartosh.service.model.Order;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Transactional
@Service
public class OrderService extends BaseService<Order, OrderEntity> implements IOrderService {
    private final static Logger LOGGER = LogManager.getLogger();

    public static final double DEFAULT_DISCOUNT = 0;
    private final ModelMapper mapper;
    private final IOrderEntityRepository iOrderEntityRepository;
    private final EquipmentMapperHelper equipmentMapperHelper;

    @Autowired
    public OrderService(IOrderEntityRepository iOrderEntityRepository, ModelMapper mapper, EquipmentMapperHelper equipmentMapperHelper) throws InstantiationException, IllegalAccessException {
        super(mapper, iOrderEntityRepository);
        this.mapper = mapper;
        this.iOrderEntityRepository = iOrderEntityRepository;
        this.equipmentMapperHelper = equipmentMapperHelper;
    }

    @Override
    public List<Order> getAll(Integer user_id) {
        List<OrderEntity> orderEntities = iOrderEntityRepository.getAll(user_id);
        List<Order> orders = mapper.map(orderEntities, new TypeToken<List<Order>>() {
        }.getType());
        return equipmentMapperHelper.mapEquipmentEntityToEquipment(orderEntities, orders);
    }

    @Override
    public Order add(Order order) {
        Date diff = new Date(order.getRentTill().getTime() - order.getRentFrom().getTime());
        Integer hours = (int) diff.getTime() / (1000 * 60 * 60);
        LOGGER.info("Calculate final cost of order where hours: " + hours + " discount: " + getDiscount(order) + " bikes cost: " + calculateBikesCost(order) + "equipment cost: " + calculateEquipmentCost(order));
        BigDecimal finalCost = calculateOrderCost(hours, getDiscount(order), calculateBikesCost(order), calculateEquipmentCost(order));
        LOGGER.info("Final cost of order: " + finalCost);
        order.setFinalCost(finalCost);
        OrderEntity insrOrderEntity = mapper.map(order, OrderEntity.class);
        insrOrderEntity = equipmentMapperHelper.mapEquipmentToEquipmentEntity(order, insrOrderEntity);
        OrderEntity orderEntity = iOrderEntityRepository.insert(insrOrderEntity);
        order = equipmentMapperHelper.mapEquipmentEntityToEquipment(orderEntity, mapper.map(orderEntity, Order.class));
        return order;

    }

    public Double getDiscount(Order order) {
        if (order.getDiscount() != null && order.getDiscount() != 0) {
            return Double.valueOf(order.getDiscount());
        } else {
            return DEFAULT_DISCOUNT;
        }
    }

    public BigDecimal calculateEquipmentCost(Order order) {
        if (order.getEquipments() != null && order.getEquipments().size() != 0) {
            return BigDecimal.valueOf(order.getEquipments().stream().mapToInt(equipment -> equipment.getCost().getCost()).sum());
        } else {
            return BigDecimal.ZERO;
        }
    }

    public BigDecimal calculateBikesCost(Order order) {
        return BigDecimal.valueOf(order.getBikes().stream().mapToInt(bike -> bike.getCost().getCost()).sum());
    }

    /**
     * Calculate final cost of order
     *
     * @param hours         hours of rent
     * @param discount      discount of rent
     * @param bikesCost     summary bikes cost
     * @param equipmentCost equipment cost
     * @return cost of order
     */
    public BigDecimal calculateOrderCost(Integer hours, Double discount, BigDecimal bikesCost, BigDecimal equipmentCost) {
        if (discount != 0) {
            return bikesCost.multiply(BigDecimal.valueOf(hours)).add(equipmentCost).multiply(BigDecimal.valueOf(discount / 100));
        } else {
            return bikesCost.multiply(BigDecimal.valueOf(hours)).add(equipmentCost);
        }
    }
}
