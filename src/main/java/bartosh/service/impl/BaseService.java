package bartosh.service.impl;


import bartosh.exception.EntitytNotFoundException;
import bartosh.repository.ICRUDRepository;
import bartosh.service.IBaseService;
import bartosh.service.model.BaseModel;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.ParameterizedType;
import java.util.List;

@Service
@Transactional
public abstract class BaseService<Model extends BaseModel, Entity> implements IBaseService<Model> {


    private ModelMapper mapper;
    private ICRUDRepository<Entity> repository;
    private final Class<Model> modelClass;
    private final Class<Entity> entityClass;

    @SuppressWarnings("unchecked")
    public BaseService(ModelMapper modelMapper, ICRUDRepository<Entity> repository) throws IllegalAccessException, InstantiationException {

        this.modelClass = (Class<Model>) ((ParameterizedType) this.getClass().getGenericSuperclass
                ()).getActualTypeArguments()[0];
        this.entityClass = (Class<Entity>) ((ParameterizedType) this.getClass().getGenericSuperclass
                ()).getActualTypeArguments()[1];
        this.mapper = modelMapper;
        this.repository = repository;
    }

    @SuppressWarnings("unchecked")
    public BaseService(ModelMapper modelMapper, ICRUDRepository<Entity> repository, PropertyMap<Model, Entity> propertyMap) throws IllegalAccessException, InstantiationException {

        this.modelClass = (Class<Model>) ((ParameterizedType) this.getClass().getGenericSuperclass
                ()).getActualTypeArguments()[0];
        this.entityClass = (Class<Entity>) ((ParameterizedType) this.getClass().getGenericSuperclass
                ()).getActualTypeArguments()[1];
        this.mapper = modelMapper;
        this.repository = repository;
        this.mapper.addMappings(propertyMap);
    }

    public List<Model> getAll() {
        return mapper.map(repository.getAll(), new TypeToken<List<Model>>() {
        }.getType());
    }

    @Override
    public Model getById(Integer id) {
        Entity entity = repository.getById(id);
        if (entity == null) {
            throw new EntitytNotFoundException("This entity doesn't exist.");
        }
        return mapper.map(entity, modelClass);
    }

    @Override
    public Model add(Model model) {
        return mapper.map(repository.insert(mapper.map(model, entityClass)), modelClass);
    }

    @Override
    public Model update(Model model) {
        return mapper.map(repository.update(mapper.map(model, entityClass)), modelClass);
    }

    @Override
    public Model delete(Integer id) {
        return mapper.map(repository.deleteById(id), modelClass);
    }
}
