package bartosh.service.impl;

import bartosh.repository.IBikeEntityRepository;
import bartosh.repository.entity.BikeEntity;
import bartosh.repository.entity.criteria.BikeEntityCriteria;
import bartosh.service.IBikeService;
import bartosh.service.mapperConfig.BikeEntityMapper;
import bartosh.service.model.Bike;
import bartosh.service.model.criteria.BikeCriteria;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
public class BikeService extends BaseService<Bike, BikeEntity> implements IBikeService {

    private IBikeEntityRepository iBikeEntityRepository;
    private ModelMapper mapper;

    @Autowired
    public BikeService(IBikeEntityRepository iBikeRepository, ModelMapper mapper) throws InstantiationException, IllegalAccessException {
        super(mapper, iBikeRepository, new BikeEntityMapper());
        this.iBikeEntityRepository = iBikeRepository;
        this.mapper = mapper;
    }

    @Override
    public List<Bike> getAll(BikeCriteria bikeCriteria) {
        return mapper.map(iBikeEntityRepository.getAll(mapper.map(bikeCriteria, BikeEntityCriteria.class)), new TypeToken<List<Bike>>() {
        }.getType());
    }

    @Override
    public Long getBikeEntityTotalRows(BikeCriteria bikeCriteria) {
        return iBikeEntityRepository.getBikeEntityTotalRows(mapper.map(bikeCriteria, BikeEntityCriteria.class));
    }


}
