package bartosh.service.impl;

import bartosh.repository.IUserEntityRepository;
import bartosh.repository.entity.Role;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service("userDetailsService")
public class UserSecurityDetailsService implements UserDetailsService {

    private IUserEntityRepository iUserEntityRepository;
    private ModelMapper mapper;

    @Autowired
    public UserSecurityDetailsService(IUserEntityRepository iUserEntityRepository, ModelMapper mapper, PasswordEncoder passwordEncoder) {
        this.iUserEntityRepository = iUserEntityRepository;
        this.mapper = mapper;
    }

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        bartosh.service.model.User myUser = mapper.map(iUserEntityRepository.findByLogin(login), bartosh.service.model.User.class);
        List<GrantedAuthority> authorities = buildUserAuthority(myUser.getRoles());
        return new User(myUser.getLogin(), myUser.getPassword(), true, true, true, true, authorities);
    }


    private List<GrantedAuthority> buildUserAuthority(Set<Role> userRoles) {

        Set<GrantedAuthority> setAuths = new HashSet<>();

        // Build user's authorities
        for (Role userRole : userRoles) {
            setAuths.add(new SimpleGrantedAuthority(userRole.name()));
        }

        return new ArrayList<>(setAuths);
    }

}
