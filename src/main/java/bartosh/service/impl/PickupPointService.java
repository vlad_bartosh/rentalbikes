package bartosh.service.impl;

import bartosh.repository.IPickupPointRepository;
import bartosh.repository.entity.PickupPointEntity;
import bartosh.service.IPickupPointService;
import bartosh.service.model.PickupPoint;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PickupPointService extends BaseService<PickupPoint, PickupPointEntity> implements IPickupPointService {


    private final IPickupPointRepository iPickupPointRepository;
    private final ModelMapper mapper;

    @Autowired
    public PickupPointService(ModelMapper mapper, IPickupPointRepository iPickupPointRepository) throws InstantiationException, IllegalAccessException {
        super(mapper, iPickupPointRepository);
        this.iPickupPointRepository = iPickupPointRepository;
        this.mapper = mapper;
    }

    @Override
    public List<PickupPoint> getAll(String name, String address) {
        return mapper.map(iPickupPointRepository.getAll(name, address), new TypeToken<List<PickupPoint>>() {
        }.getType());
    }

}
