package bartosh.service.impl;

import bartosh.repository.IUserEntityRepository;
import bartosh.repository.entity.UserEntity;
import bartosh.service.IUserService;
import bartosh.service.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class UserService extends BaseService<User, UserEntity> implements IUserService {

    private final IUserEntityRepository iUserEntityRepository;
    private final ModelMapper mapper;

    @Autowired
    public UserService(ModelMapper mapper, IUserEntityRepository iUserEntityRepository) throws IllegalAccessException, InstantiationException {
        super(mapper, iUserEntityRepository);
        this.iUserEntityRepository = iUserEntityRepository;
        this.mapper = mapper;
    }

    @Override
    public User getUserByCriteria(String login, String email) {
        return iUserEntityRepository.getUserByCriteria(login, email) != null ?
                mapper.map(iUserEntityRepository.getUserByCriteria(login, email), User.class) :
                null;
    }

    public Boolean loginIsExist(String login) {
        return iUserEntityRepository.findByLogin(login) != null;
    }

    @Override
    public Boolean emailIsExist(String email) {
        return iUserEntityRepository.emailIsExist(email);
    }
}
