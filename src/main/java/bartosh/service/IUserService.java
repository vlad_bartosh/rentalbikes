package bartosh.service;

import bartosh.service.model.User;

public interface IUserService extends IBaseService<User> {
    User getUserByCriteria(String login, String email);

    Boolean loginIsExist(String login);

    Boolean emailIsExist(String email);
}
