package bartosh.service.model.criteria;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CostCriteria {
    private String typeCost;
    private Integer minCost;
    private Integer maxCost;
}
