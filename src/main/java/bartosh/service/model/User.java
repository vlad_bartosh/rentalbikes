package bartosh.service.model;

import bartosh.repository.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Set;


@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User extends BaseModel {
    private String login;
    private String email;
    private String telephone;
    private String password;
    private Set<Role> roles;
}
