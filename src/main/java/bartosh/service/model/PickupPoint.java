package bartosh.service.model;

import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PickupPoint extends BaseModel {
    private String name;
    private String address;
    private String latitude;
    private String longtitude;
}
