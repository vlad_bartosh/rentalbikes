package bartosh.service.model;

import lombok.*;


@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Cost extends BaseModel {
    private Integer id;
    private String typeCost;
    private Integer cost;

}
