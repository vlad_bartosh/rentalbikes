package bartosh.service.model.equipment;

import bartosh.service.model.Cost;
import bartosh.service.model.PickupPoint;
import bartosh.service.model.Producer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class BabySeat extends Equipment {
    public BabySeat(String name, Producer producer, Cost cost, Integer count, PickupPoint pickupPoint) {
        super(name, producer, cost, count, pickupPoint);
    }
}
