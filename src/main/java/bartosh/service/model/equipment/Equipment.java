package bartosh.service.model.equipment;

import bartosh.service.model.BaseModel;
import bartosh.service.model.Cost;
import bartosh.service.model.PickupPoint;
import bartosh.service.model.Producer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class Equipment extends BaseModel {
    private String name;
    private Producer producer;
    private Cost cost;
    private Integer count;
    private PickupPoint pickupPoint;
}
