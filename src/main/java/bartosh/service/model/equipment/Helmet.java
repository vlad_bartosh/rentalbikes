package bartosh.service.model.equipment;

import bartosh.repository.entity.AgeType;
import bartosh.repository.entity.Color;
import bartosh.service.model.Cost;
import bartosh.service.model.PickupPoint;
import bartosh.service.model.Producer;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class Helmet extends Equipment {
    private AgeType ageType;
    private Color color;

    public Helmet(String name, Producer producer, Cost cost, Integer count, PickupPoint pickupPoint, AgeType ageType, Color color) {
        super(name, producer, cost, count, pickupPoint);
        this.ageType = ageType;
        this.color = color;
    }

}
