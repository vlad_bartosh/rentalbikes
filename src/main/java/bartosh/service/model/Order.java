package bartosh.service.model;

import bartosh.service.model.equipment.Equipment;
import lombok.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Order extends BaseModel {
    private PickupPoint pickupPoint;
    private PickupPoint pickoffPoint;
    private Date rentFrom;
    private Date rentTill;
    private List<Bike> bikes;
    private List<Equipment> equipments;
    private User user;
    private BigDecimal finalCost;
    private Integer discount;
}
