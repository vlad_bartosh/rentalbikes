package bartosh.service.model;

import bartosh.repository.entity.*;
import lombok.*;

import java.util.List;

/**
 * Bike - class-entity, used to do different logical and transactional operation through  service layer.
 */

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Bike extends BaseModel {
    private Producer producer;
    private TypeBike typeBike;
    private Color color;
    private Status status;
    private Sex sex;
    private AgeType ageType;
    private PickupPoint pickupPoint;
    private Cost cost;
    private List<Order> order;

}
