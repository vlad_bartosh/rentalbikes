package bartosh.service.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * BaseModel - class, that contain the common field  in model layer.
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class BaseModel {
    private Integer id;
}
