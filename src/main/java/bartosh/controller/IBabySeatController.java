package bartosh.controller;

import bartosh.controller.dto.equipment.BabySeatCreateUpdateDTO;
import bartosh.controller.dto.equipment.BabySeatDTO;
import bartosh.controller.dto.equipment.EquipmentDTOCriteria;

import java.util.List;

/**
 * IBabySeatController - class-controller, that represent endpoints of BabySeat entity
 */

public interface IBabySeatController extends IBaseController<BabySeatDTO, BabySeatCreateUpdateDTO> {

    /**
     * @param equipmentDTOCriteria - criteria which is used to get entities from database
     * @return list of {@link BabySeatDTO}
     */
    List<BabySeatDTO> getAll(EquipmentDTOCriteria equipmentDTOCriteria);
}
