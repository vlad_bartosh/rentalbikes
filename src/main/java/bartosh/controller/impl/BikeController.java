package bartosh.controller.impl;

import bartosh.controller.IBikeController;
import bartosh.controller.dto.BikeCreateUpdateDTO;
import bartosh.controller.dto.BikeCriteriaDTO;
import bartosh.controller.dto.BikeDTO;
import bartosh.exception.InvalidParameterException;
import bartosh.service.IBikeService;
import bartosh.service.ICostService;
import bartosh.service.IPickupPointService;
import bartosh.service.IProducerService;
import bartosh.service.model.Bike;
import bartosh.service.model.criteria.BikeCriteria;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/bikes")
public class BikeController implements IBikeController {
    private final static Logger LOGGER = LogManager.getLogger();

    private final IBikeService iBikeService;
    private final ModelMapper mapper;
    private final IPickupPointService iPickupPointService;
    private final ICostService iCostService;
    private final IProducerService iProducerService;

    @Autowired
    public BikeController(IBikeService iBikeService, ModelMapper mapper, IPickupPointService iPickupPointService, ICostService iCostService, IProducerService iProducerService) {
        this.iBikeService = iBikeService;
        this.mapper = mapper;
        this.iPickupPointService = iPickupPointService;
        this.iCostService = iCostService;
        this.iProducerService = iProducerService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<BikeDTO> getAll(BikeCriteriaDTO bikeCriteriaDTO) {
        LOGGER.info("Get all bikes with criteria" + bikeCriteriaDTO);
        if (bikeCriteriaDTO.getMinCost() != null && bikeCriteriaDTO.getMaxCost() != null) {
            if (bikeCriteriaDTO.getMinCost() > bikeCriteriaDTO.getMaxCost()) {
                throw new InvalidParameterException("MinCost parameter more then maxCost parameter");
            }
        }
        if (bikeCriteriaDTO.getRentFrom() != null && bikeCriteriaDTO.getRentTill() != null) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            Date currentDate = new Date();
            dateFormat.format(currentDate);
            if (bikeCriteriaDTO.getRentFrom().before(currentDate)) {
                throw new InvalidParameterException("Incorrect Date");
            }
            if (bikeCriteriaDTO.getRentTill().before(bikeCriteriaDTO.getRentFrom())) {
                throw new InvalidParameterException("Incorrect Date");
            }
            Date diff = new Date(bikeCriteriaDTO.getRentTill().getTime() - bikeCriteriaDTO.getRentFrom().getTime());
            int hours = (int) Math.ceil(diff.getTime() / (1000 * 60 * 60));
            if (hours == 0) {
                throw new InvalidParameterException("Incorrect Date");
            }
        }
        BikeCriteria bikeCriteria = mapper.map(bikeCriteriaDTO, BikeCriteria.class);
        if (bikeCriteriaDTO.getPickupPoint_id() != null) {
            bikeCriteria.setPickupPoint(iPickupPointService.getById(Integer.parseInt(bikeCriteriaDTO.getPickupPoint_id())).getName());
        }
        return mapper.map(iBikeService.getAll(bikeCriteria), new TypeToken<List<BikeDTO>>() {
        }.getType());
    }

    @GetMapping("/rows")
    @ResponseStatus(HttpStatus.OK)
    public Long getBikeEntityTotalRows(BikeCriteriaDTO bikeCriteriaDTO) {
        LOGGER.info("Get total rows of bikes with criteria" + bikeCriteriaDTO);
        BikeCriteria bikeCriteria = mapper.map(bikeCriteriaDTO, BikeCriteria.class);
        if (bikeCriteriaDTO.getPickupPoint_id() != null) {
            bikeCriteria.setPickupPoint(iPickupPointService.getById(Integer.parseInt(bikeCriteriaDTO.getPickupPoint_id())).getName());
        }
        return iBikeService.getBikeEntityTotalRows(bikeCriteria);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public BikeDTO getById(@PathVariable Integer id) {
        LOGGER.info("Get bike with id = " + id);
        return mapper.map(iBikeService.getById(id), BikeDTO.class);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BikeDTO add(@RequestBody @Valid BikeCreateUpdateDTO bikeDTO) {
        LOGGER.info("Add bike where BikeCreateUpdateDTO: " + bikeDTO);
        Bike bike = mapper.map(bikeDTO, Bike.class);
        bike.setPickupPoint(iPickupPointService.getById(bikeDTO.getPickupPoint_id()));
        bike.setCost(iCostService.getById(bikeDTO.getCost_id()));
        bike.setProducer(iProducerService.getById(bikeDTO.getProducer_id()));
        return mapper.map(iBikeService.add(bike), BikeDTO.class);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public BikeDTO update(@RequestBody BikeCreateUpdateDTO bikeDTO, @PathVariable Integer id) {
        LOGGER.info("Update bike where BikeCreateUpdateDTO: " + bikeDTO + " and id: " + id);
        bikeDTO.setId(id);
        Bike bike = mapper.map(bikeDTO, Bike.class);
        bike.setPickupPoint(iPickupPointService.getById(bikeDTO.getPickupPoint_id()));
        bike.setCost(iCostService.getById(bikeDTO.getCost_id()));
        bike.setProducer(iProducerService.getById(bikeDTO.getProducer_id()));
        return mapper.map(iBikeService.update(bike), BikeDTO.class);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public BikeDTO delete(@PathVariable Integer id) {
        LOGGER.info("Delete bike where id: " + id);
        return mapper.map(iBikeService.delete(id), BikeDTO.class);
    }
}
