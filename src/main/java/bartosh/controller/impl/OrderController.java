package bartosh.controller.impl;

import bartosh.controller.IOrderController;
import bartosh.controller.dto.OrderCreateUpdateDTO;
import bartosh.controller.dto.OrderDTO;
import bartosh.service.*;

import bartosh.service.EmailSender;

import bartosh.service.impl.YandexEmailSender;
import bartosh.service.mapperConfig.EquipmentMapperHelper;
import bartosh.service.model.Bike;
import bartosh.service.model.Order;
import bartosh.service.model.equipment.Equipment;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/orders")
public class OrderController implements IOrderController {
    private final static Logger LOGGER = LogManager.getLogger();

    private final IOrderService iOrderService;
    private final ModelMapper mapper;
    private final IPickupPointService iPickupPointService;
    private final IUserService iUserService;
    private final IBikeService iBikeService;
    private final IHelmetService iHelmetService;
    private final IBasketService iBasketService;
    private final IBabySeatService iBabySeatService;
    private final EquipmentMapperHelper equipmentMapperHelper;
    private final EmailSender emailSender;

    @Autowired
    public OrderController(IOrderService iOrderService, ModelMapper mapper, IPickupPointService iPickupPointService, IUserService iUserService, IBikeService iBikeService, IHelmetService iHelmetService, IBasketService iBasketService, IBabySeatService iBabySeatService, YandexEmailSender emailSender, EquipmentMapperHelper equipmentMapperHelper, YandexEmailSender sendEmailAboutOrder1) {
        this.iOrderService = iOrderService;
        this.mapper = mapper;
        this.iPickupPointService = iPickupPointService;
        this.iUserService = iUserService;
        this.iBikeService = iBikeService;
        this.iHelmetService = iHelmetService;
        this.iBasketService = iBasketService;
        this.iBabySeatService = iBabySeatService;
        this.equipmentMapperHelper = equipmentMapperHelper;
        this.emailSender = sendEmailAboutOrder1;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<OrderDTO> getAll(Integer user_id) {
        LOGGER.info("Get OrderDTO where user id: " + user_id);
        List<Order> orders = iOrderService.getAll(user_id);
        List<OrderDTO> ordersDTO = mapper.map(orders, new TypeToken<List<OrderDTO>>() {
        }.getType());
        return equipmentMapperHelper.mapEquipmentToEquipmentDTO(orders, ordersDTO);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public OrderDTO getById(@PathVariable Integer id) {
        LOGGER.info("Get OrderDTO where id: " + id);
        return mapper.map(iOrderService.getById(id), OrderDTO.class);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public OrderDTO add(@RequestBody OrderCreateUpdateDTO orderDTO) {
        LOGGER.info("Add Order where OrderDTO: " + orderDTO);
        Order order = mapper.map(orderDTO, Order.class);
        order.setPickupPoint(iPickupPointService.getById(orderDTO.getPickupPoint_id()));
        order.setPickoffPoint(iPickupPointService.getById(orderDTO.getPickoffPoint_id()));
        order.setUser(iUserService.getById(orderDTO.getUser_id()));
        List<Bike> bikes = orderDTO.getBikes_id().stream().map(iBikeService::getById).collect(Collectors.toList());
        List<Equipment> listEquipment = new ArrayList<>();
        if (orderDTO.getHelmet_id() != null) {
            listEquipment.add(iHelmetService.getById(orderDTO.getHelmet_id()));
        }
        if (orderDTO.getBasket_id() != null) {
            listEquipment.add(iBasketService.getById(orderDTO.getBasket_id()));
        }
        if (orderDTO.getBabySeat_id() != null) {
            listEquipment.add(iBabySeatService.getById(orderDTO.getBabySeat_id()));
        }
        order.setEquipments(listEquipment);
        order.setBikes(bikes);
        Order returnedOrder = iOrderService.add(order);
        OrderDTO returnedOrderDTO = mapper.map(returnedOrder, OrderDTO.class);
        returnedOrderDTO = equipmentMapperHelper.mapEquipmentToEquipmentDTO(returnedOrder, returnedOrderDTO);
        return returnedOrderDTO;
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public OrderDTO update(@RequestBody OrderCreateUpdateDTO orderDTO, @PathVariable Integer id) {
        LOGGER.info("Update Order with id: " + id + "where orderDTO: " + orderDTO);
        orderDTO.setId(id);
        Order order = mapper.map(orderDTO, Order.class);
        order.setPickupPoint(iPickupPointService.getById(orderDTO.getPickupPoint_id()));
        order.setPickoffPoint(iPickupPointService.getById(orderDTO.getPickoffPoint_id()));
        order.setUser(iUserService.getById(orderDTO.getUser_id()));
        return mapper.map(iOrderService.update(order), OrderDTO.class);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public OrderDTO delete(@PathVariable Integer id) {
        LOGGER.info("Delete Order where id: " + id);
        return mapper.map(iOrderService.delete(id), OrderDTO.class);
    }

    @PostMapping("/sendEmail")
    @ResponseStatus(HttpStatus.OK)
    public OrderDTO sendEmailToEmployee(@RequestBody OrderDTO orderDTO) {
        LOGGER.info("Send email where OrderDTO: " + orderDTO);
        Order order = mapper.map(orderDTO, Order.class);
        order = equipmentMapperHelper.mapEquipmentDTOToEquipment(orderDTO, order);
        emailSender.sendToEmployee(order);
        return orderDTO;
    }

    @GetMapping("/user")
    @ResponseStatus(HttpStatus.OK)
    public List<OrderDTO> getOrderByUserId(Integer user_id) {
        LOGGER.info("Get Order where user id: " + user_id);
        List<Order> orders = iOrderService.getAll(user_id);
        List<OrderDTO> ordersDTO = mapper.map(orders, new TypeToken<List<OrderDTO>>() {
        }.getType());
        return equipmentMapperHelper.mapEquipmentToEquipmentDTO(orders, ordersDTO);
    }
}
