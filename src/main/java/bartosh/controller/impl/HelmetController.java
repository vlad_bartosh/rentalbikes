package bartosh.controller.impl;

import bartosh.controller.IHelmetController;
import bartosh.controller.dto.equipment.EquipmentDTOCriteria;
import bartosh.controller.dto.equipment.HelmetCreateUpdateDTO;
import bartosh.controller.dto.equipment.HelmetDTO;
import bartosh.service.ICostService;
import bartosh.service.IHelmetService;
import bartosh.service.IPickupPointService;
import bartosh.service.IProducerService;
import bartosh.service.mapperConfig.EquipmentMapperHelper;
import bartosh.service.model.criteria.EquipmentCriteria;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/helmets")
public class HelmetController implements IHelmetController {
    private final IHelmetService iHelmetService;
    private final IProducerService iProducerService;
    private final IPickupPointService iPickupPointService;
    private final ICostService iCostService;
    private final ModelMapper mapper;

    @Autowired
    public HelmetController(IHelmetService iHelmetService, IProducerService iProducerService, IPickupPointService iPickupPointService, ICostService iCostService, ModelMapper mapper) {
        this.iHelmetService = iHelmetService;
        this.iProducerService = iProducerService;
        this.iPickupPointService = iPickupPointService;
        this.iCostService = iCostService;
        this.mapper = mapper;
    }


    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<HelmetDTO> getAll(EquipmentDTOCriteria equipmentDTOCriteria) {
        List<bartosh.service.model.equipment.Helmet> helmets = iHelmetService.getAll(mapper.map(equipmentDTOCriteria, EquipmentCriteria.class));
        List<HelmetDTO> result = mapper.map(helmets, new TypeToken<List<HelmetDTO>>() {
        }.getType());
        EquipmentMapperHelper equipmentMapperHelper = new EquipmentMapperHelper(mapper);
        result = equipmentMapperHelper.helmetDTOPickupPointsMapping(helmets, result);
        return result;
    }


    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public HelmetDTO getById(@PathVariable Integer id) {
        return mapper.map(iHelmetService.getById(id), HelmetDTO.class);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public HelmetDTO add(@RequestBody HelmetCreateUpdateDTO helmetCreateUpdateDTO) {
        bartosh.service.model.equipment.Helmet helmet = mapper.map(helmetCreateUpdateDTO, bartosh.service.model.equipment.Helmet.class);
        helmet.setProducer(iProducerService.getById(helmetCreateUpdateDTO.getProducer_id()));
        helmet.setPickupPoint(iPickupPointService.getById(helmetCreateUpdateDTO.getPickupPointDTO_id()));
        helmet.setCost(iCostService.getById(helmetCreateUpdateDTO.getCost_id()));
        bartosh.service.model.equipment.Helmet helmetSrv = iHelmetService.add(helmet);
        return mapper.map(helmetSrv, HelmetDTO.class);
    }


    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public HelmetDTO update(@RequestBody HelmetCreateUpdateDTO helmetCreateUpdateDTO, @PathVariable Integer id) {
        helmetCreateUpdateDTO.setId(id);
        bartosh.service.model.equipment.Helmet helmet = mapper.map(helmetCreateUpdateDTO, bartosh.service.model.equipment.Helmet.class);
        helmet.setProducer(iProducerService.getById(helmetCreateUpdateDTO.getProducer_id()));
        helmet.setPickupPoint(iPickupPointService.getById(helmetCreateUpdateDTO.getPickupPointDTO_id()));
        helmet.setCost(iCostService.getById(helmetCreateUpdateDTO.getCost_id()));
        return mapper.map(iHelmetService.update(helmet), HelmetDTO.class);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public HelmetDTO delete(@PathVariable Integer id) {
        return mapper.map(iHelmetService.delete(id), HelmetDTO.class);
    }

    @GetMapping("/pickup-point/{id}")
    @ResponseStatus(HttpStatus.OK)
    public HelmetDTO getHelmetsByPickupPointId(@PathVariable Integer id) {
        return null;
    }

}
