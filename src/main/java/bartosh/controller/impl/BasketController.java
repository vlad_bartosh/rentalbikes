package bartosh.controller.impl;

import bartosh.controller.IBasketController;
import bartosh.controller.dto.PickupPointDTO;
import bartosh.controller.dto.equipment.BasketCreateUpdateDTO;
import bartosh.controller.dto.equipment.BasketDTO;
import bartosh.controller.dto.equipment.EquipmentDTOCriteria;
import bartosh.service.IBasketService;
import bartosh.service.ICostService;
import bartosh.service.IPickupPointService;
import bartosh.service.IProducerService;
import bartosh.service.model.criteria.EquipmentCriteria;
import bartosh.service.model.equipment.Basket;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/baskets")
public class BasketController implements IBasketController {
    private final static Logger LOGGER = LogManager.getLogger();

    private final IBasketService iBasketService;
    private final IProducerService iProducerService;
    private final IPickupPointService iPickupPointService;
    private final ICostService iCostService;
    private final ModelMapper mapper;

    public BasketController(IBasketService iBasketService, IProducerService iProducerService, IPickupPointService iPickupPointService, ICostService iCostService, ModelMapper mapper) {
        this.iBasketService = iBasketService;
        this.iProducerService = iProducerService;
        this.iPickupPointService = iPickupPointService;
        this.iCostService = iCostService;
        this.mapper = mapper;
    }

    @Override
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<BasketDTO> getAll(EquipmentDTOCriteria equipmentDTOCriteria) {
        LOGGER.info("Get BasketDTO with EquipmentDTOCriteria: " + equipmentDTOCriteria);
        List<Basket> baskets = iBasketService.getAll(mapper.map(equipmentDTOCriteria, EquipmentCriteria.class));
        List<BasketDTO> basketsDTO = mapper.map(baskets, new TypeToken<List<BasketDTO>>() {
        }.getType());
        Map<Integer, PickupPointDTO> basketDTOMap = baskets.stream().collect(Collectors.toMap(
                Basket::getId, basket -> mapper.map(basket.getPickupPoint(), PickupPointDTO.class)));
        basketsDTO = basketsDTO.stream().peek(basketDTO -> basketDTO.setPickupPointDTO(basketDTOMap.get(basketDTO.getId()))).collect(Collectors.toList());
        return basketsDTO;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public BasketDTO getById(@PathVariable Integer id) {
        LOGGER.info("Get BasketDTO where id: " + id);
        return mapper.map(iBasketService.getById(id), BasketDTO.class);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BasketDTO add(@RequestBody BasketCreateUpdateDTO basketCreateUpdateDTO) {
        LOGGER.info("Add Basket where BasketCreateUpdateDTO: " + basketCreateUpdateDTO);
        Basket basket = mapper.map(basketCreateUpdateDTO, Basket.class);
        basket.setCost(iCostService.getById(basketCreateUpdateDTO.getCost_id()));
        basket.setProducer(iProducerService.getById(basketCreateUpdateDTO.getProducer_id()));
        basket.setPickupPoint(iPickupPointService.getById(basketCreateUpdateDTO.getPickupPointDTO_id()));
        return mapper.map(iBasketService.add(basket), BasketDTO.class);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public BasketDTO update(@RequestBody BasketCreateUpdateDTO basketCreateUpdateDTO, @PathVariable Integer id) {
        LOGGER.info("Update Basket where BasketCreateUpdateDTO: " + basketCreateUpdateDTO + " and id: " + id);
        basketCreateUpdateDTO.setId(id);
        Basket basket = mapper.map(basketCreateUpdateDTO, Basket.class);
        basket.setCost(iCostService.getById(basketCreateUpdateDTO.getCost_id()));
        basket.setProducer(iProducerService.getById(basketCreateUpdateDTO.getProducer_id()));
        basket.setPickupPoint(iPickupPointService.getById(basketCreateUpdateDTO.getPickupPointDTO_id()));
        return mapper.map(iBasketService.update(basket), BasketDTO.class);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public BasketDTO delete(@PathVariable Integer id) {
        LOGGER.info("Delete Basket where id: " + id);
        return mapper.map(iBasketService.delete(id), BasketDTO.class);
    }
}
