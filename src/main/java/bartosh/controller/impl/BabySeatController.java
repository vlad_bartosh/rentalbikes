package bartosh.controller.impl;

import bartosh.controller.IBabySeatController;
import bartosh.controller.dto.PickupPointDTO;
import bartosh.controller.dto.equipment.BabySeatCreateUpdateDTO;
import bartosh.controller.dto.equipment.BabySeatDTO;
import bartosh.controller.dto.equipment.EquipmentDTOCriteria;
import bartosh.service.IBabySeatService;
import bartosh.service.ICostService;
import bartosh.service.IPickupPointService;
import bartosh.service.IProducerService;
import bartosh.service.model.criteria.EquipmentCriteria;
import bartosh.service.model.equipment.BabySeat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RequestMapping("/baby-seats")
@RestController
public class BabySeatController implements IBabySeatController {
    private final static Logger LOGGER = LogManager.getLogger();

    private final IBabySeatService iBabySeatService;
    private final IProducerService iProducerService;
    private final IPickupPointService iPickupPointService;
    private final ICostService iCostService;
    private final ModelMapper mapper;

    public BabySeatController(IBabySeatService iBabySeatService, IProducerService iProducerService, IPickupPointService iPickupPointService, ICostService iCostService, ModelMapper mapper) {
        this.iBabySeatService = iBabySeatService;
        this.iProducerService = iProducerService;
        this.iPickupPointService = iPickupPointService;
        this.iCostService = iCostService;
        this.mapper = mapper;
    }


    @Override
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<BabySeatDTO> getAll(EquipmentDTOCriteria equipmentDTOCriteria) {
        LOGGER.info("Get BabySeatDTO with EquipmentDTOCriteria: " + equipmentDTOCriteria);
        List<BabySeat> babySeats = iBabySeatService.getAll(mapper.map(equipmentDTOCriteria, EquipmentCriteria.class));
        List<BabySeatDTO> babySeatsDTO = mapper.map(babySeats, new TypeToken<List<BabySeatDTO>>() {
        }.getType());
        Map<Integer, PickupPointDTO> pickupPointDTOMap = babySeats.stream().collect(Collectors.toMap(
                BabySeat::getId, babySeat -> mapper.map(babySeat.getPickupPoint(), PickupPointDTO.class)));
        babySeatsDTO = babySeatsDTO.stream().peek(babySeatDTO -> babySeatDTO.setPickupPointDTO(pickupPointDTOMap.get(babySeatDTO.getId()))).collect(Collectors.toList());
        return babySeatsDTO;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public BabySeatDTO getById(@PathVariable Integer id) {
        LOGGER.info("Get BabySeatDTO where id: " + id);
        return mapper.map(iBabySeatService.getById(id), BabySeatDTO.class);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BabySeatDTO add(@RequestBody BabySeatCreateUpdateDTO babySeatCreateUpdateDTO) {
        LOGGER.info("Add BabySeatDTO where BabySeatUpdateDTO: " + babySeatCreateUpdateDTO);
        BabySeat babySeat = mapper.map(babySeatCreateUpdateDTO, BabySeat.class);
        babySeat.setCost(iCostService.getById(babySeatCreateUpdateDTO.getCost_id()));
        babySeat.setProducer(iProducerService.getById(babySeatCreateUpdateDTO.getProducer_id()));
        babySeat.setPickupPoint(iPickupPointService.getById(babySeatCreateUpdateDTO.getPickupPointDTO_id()));
        return mapper.map(iBabySeatService.add(babySeat), BabySeatDTO.class);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public BabySeatDTO update(@RequestBody BabySeatCreateUpdateDTO babySeatCreateUpdateDTO, @PathVariable Integer id) {
        LOGGER.info("Update BabySeatDTO where babySeatCreateUpdateDTO: " + babySeatCreateUpdateDTO + " and id: " + id);
        babySeatCreateUpdateDTO.setId(id);
        BabySeat babySeat = mapper.map(babySeatCreateUpdateDTO, BabySeat.class);
        babySeat.setCost(iCostService.getById(babySeatCreateUpdateDTO.getCost_id()));
        babySeat.setProducer(iProducerService.getById(babySeatCreateUpdateDTO.getProducer_id()));
        babySeat.setPickupPoint(iPickupPointService.getById(babySeatCreateUpdateDTO.getPickupPointDTO_id()));
        return mapper.map(iBabySeatService.update(babySeat), BabySeatDTO.class);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.OK)
    public BabySeatDTO delete(@PathVariable Integer id) {
        LOGGER.info("Delete BabySeatDTO where id: " + id);
        return mapper.map(iBabySeatService.delete(id), BabySeatDTO.class);
    }
}
