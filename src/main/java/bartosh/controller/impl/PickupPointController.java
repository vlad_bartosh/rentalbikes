package bartosh.controller.impl;

import bartosh.controller.IPickupPointController;
import bartosh.controller.dto.PickupPointDTO;
import bartosh.service.IPickupPointService;
import bartosh.service.model.PickupPoint;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/pickup-points")
public class PickupPointController implements IPickupPointController {
    private final static Logger LOGGER = LogManager.getLogger();

    private IPickupPointService iPickupPointService;
    private ModelMapper mapper;

    @Autowired
    public PickupPointController(IPickupPointService iPickupPointService, ModelMapper mapper) {
        this.iPickupPointService = iPickupPointService;
        this.mapper = mapper;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<PickupPointDTO> getAll(String name, String address) {
        LOGGER.info("Get list of PickupPointDTO where name: " + name + " and address: " + address);
        return mapper.map(iPickupPointService.getAll(name, address), new TypeToken<List<PickupPointDTO>>() {
        }.getType());
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PickupPointDTO getById(@PathVariable Integer id) {
        LOGGER.info("Get PickupPointDTO where id: " + id);
        return mapper.map(iPickupPointService.getById(id), PickupPointDTO.class);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PickupPointDTO add(@RequestBody PickupPointDTO pickupPointDTO) {
        LOGGER.info("Add PickupPointDTO: " + pickupPointDTO);
        return mapper.map(iPickupPointService.add(mapper.map(pickupPointDTO, PickupPoint.class)), PickupPointDTO.class);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PickupPointDTO update(@RequestBody PickupPointDTO pickupPointDTO, @PathVariable Integer id) {
        LOGGER.info("Update PickupPoint where id: " + id + "PickupPointDTO: " + pickupPointDTO);
        pickupPointDTO.setId(id);
        return mapper.map(iPickupPointService.update(mapper.map(pickupPointDTO, PickupPoint.class)), PickupPointDTO.class);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public PickupPointDTO delete(@PathVariable Integer id) {
        LOGGER.info("Delete PickupPoint where id: " + id);
        return mapper.map(iPickupPointService.delete(id), PickupPointDTO.class);
    }
}
