package bartosh.controller.impl;

import bartosh.controller.dto.UserDTO;
import bartosh.exception.IncorrectPasswordException;
import bartosh.exception.RegistrationParameterIsExistException;
import bartosh.repository.entity.Role;
import bartosh.service.IUserService;
import bartosh.service.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.HashSet;


@RestController
@RequestMapping("/user")
public class UserController {
    private final static Logger LOGGER = LogManager.getLogger();

    private final IUserService iUserService;
    private final ModelMapper mapper;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserController(IUserService iUserService, ModelMapper mapper, PasswordEncoder passwordEncoder) {
        this.iUserService = iUserService;
        this.mapper = mapper;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDTO getById(@PathVariable Integer id) {
        LOGGER.info("Get user where id: " + id);
        return mapper.map(iUserService.getById(id), UserDTO.class);
    }

    @PostMapping("/sign-up")
    @ResponseStatus(HttpStatus.CREATED)
    public UserDTO registration(@RequestBody UserDTO userDTO) {
        LOGGER.info("Registration of userDTO: " + userDTO);
        if (iUserService.getUserByCriteria(userDTO.getLogin(), null) != null) {
            throw new RegistrationParameterIsExistException("This login is exist");
        }
        if (iUserService.getUserByCriteria(null, userDTO.getEmail()) != null) {
            throw new RegistrationParameterIsExistException("User with this email already exist");
        }
        userDTO.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        userDTO.setRoles(new HashSet<>(Collections.singletonList(Role.ROLE_USER)));
        return mapper.map(iUserService.add(mapper.map(userDTO, User.class)), UserDTO.class);
    }

    @GetMapping("/login")
    @ResponseStatus(HttpStatus.OK)
    public UserDTO loginUser(String login, String password) {

        if (!iUserService.loginIsExist(login)) {
            throw new RegistrationParameterIsExistException("Incorrect login");
        }
        UserDTO result = mapper.map(iUserService.getUserByCriteria(login, null), UserDTO.class);
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if (!encoder.matches(password, result.getPassword())) {
            throw new IncorrectPasswordException("Incorrect password");
        }
        LOGGER.info("Login user with login: " + login + " and password: " + password);
        return result;
    }

}
