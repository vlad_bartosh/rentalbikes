package bartosh.controller;

import bartosh.controller.dto.equipment.EquipmentDTOCriteria;
import bartosh.controller.dto.equipment.HelmetCreateUpdateDTO;
import bartosh.controller.dto.equipment.HelmetDTO;

import java.util.List;

/**
 * IHelmetController - class-controller that represent endpoints of Helmet entity
 */
public interface IHelmetController extends IBaseController<HelmetDTO, HelmetCreateUpdateDTO> {
    /**
     * Get HelmetDTO from database by criteria
     *
     * @param equipmentDTOCriteria - criteria of Helmet entity
     * @return list of {@link HelmetDTO}
     */
    List<HelmetDTO> getAll(EquipmentDTOCriteria equipmentDTOCriteria);

}
