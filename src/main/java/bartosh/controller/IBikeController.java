package bartosh.controller;

import bartosh.controller.dto.BikeCreateUpdateDTO;
import bartosh.controller.dto.BikeCriteriaDTO;
import bartosh.controller.dto.BikeDTO;

import java.util.List;

/**
 * IBikeController - class-controller that represent endpoints of Bike entity
 */
public interface IBikeController extends IBaseController<BikeDTO, BikeCreateUpdateDTO> {
    /**
     * Get BikeDTO
     *
     * @param bikeCriteriaDTO - criteria of Bike entity
     * @return list of {@link BikeDTO}
     */
    List<BikeDTO> getAll(BikeCriteriaDTO bikeCriteriaDTO);

    /**
     * Get total rows of Bike entity by criteria
     *
     * @param bikeCriteriaDTO - criteria of Bike entity
     * @return number of rows
     */
    Long getBikeEntityTotalRows(BikeCriteriaDTO bikeCriteriaDTO);
}
