package bartosh.controller;

import bartosh.controller.dto.PickupPointDTO;

import java.util.List;

/**
 * IPickupPointController - class-controller that represent endpoints of PickupPoint entity
 */

public interface IPickupPointController {
    /**
     * Get PickupPoint by name and address
     *
     * @param name    - name of PickupPoint
     * @param address - address of PickupPoint
     * @return list of PickupPoints
     */
    List<PickupPointDTO> getAll(String name, String address);

    /**
     * Get PickupPointDTO by id
     *
     * @param id - id of PickupPoint
     * @return PickupPointDTO
     */
    PickupPointDTO getById(Integer id);

    /**
     * Add PickupPoint to database
     *
     * @param pickupPointDTO - PickupPointDTO that add to database
     * @return PickupPoint
     */
    PickupPointDTO add(PickupPointDTO pickupPointDTO);

    /**
     * Update PickupPoint
     *
     * @param pickupPointDTO - PickupPointDTO with parameters to update
     * @param id             - id of PickupPoint to update
     * @return
     */
    PickupPointDTO update(PickupPointDTO pickupPointDTO, Integer id);

    /**
     * Delete PickupPoint
     *
     * @param id - id of PickupPoint
     * @return PickupPoint
     */
    PickupPointDTO delete(Integer id);
}
