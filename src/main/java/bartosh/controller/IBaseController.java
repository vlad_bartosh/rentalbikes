package bartosh.controller;

/**
 * Base controller to classes in controller layer
 *
 * @param <DTO>      - Data transfer object class
 * @param <CrUpdDTO> class only to create and update entity
 */

public interface IBaseController<DTO, CrUpdDTO> {
    /**
     * Get DTO from database by id
     *
     * @param id - id of DTO
     * @return DTO
     */
    DTO getById(Integer id);

    /**
     * Add DTO to database
     *
     * @param dto - DTO for create update
     * @return - DTO
     */
    DTO add(CrUpdDTO dto);

    /**
     * Update DTO
     *
     * @param dto DTO for create update
     * @param id  id of object
     * @return DTO
     */
    DTO update(CrUpdDTO dto, Integer id);

    /**
     * Delete DTO by id
     *
     * @param id id of DTO
     * @return data transfer object
     */
    DTO delete(Integer id);
}
