package bartosh.controller.dto;

import bartosh.repository.entity.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BikeCreateUpdateDTO {
    @Min(0)
    private Integer id;
    @NotNull
    private Integer producer_id;
    @NotNull(message = "you should add the typeBike!!!")
    private TypeBike typeBike;
    @NotNull(message = "you should add the color!!!")
    private Color color;
    @NotNull
    private Status status;
    @NotNull
    private Sex sex;
    @NotNull
    private AgeType ageType;
    @NotNull
    private Integer pickupPoint_id;
    @NotNull
    private Integer cost_id;
}

