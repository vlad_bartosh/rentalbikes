package bartosh.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CostDTO {
    @Min(0)
    private Integer id;
    @NotNull
    private String typeCost;
    @Min(0)
    @NotNull(message = "cost should not be null")
    private Integer cost;
}
