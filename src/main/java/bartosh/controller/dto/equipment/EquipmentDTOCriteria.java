package bartosh.controller.dto.equipment;

import bartosh.repository.entity.AgeType;
import bartosh.repository.entity.Color;
import bartosh.repository.entity.ProducerEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EquipmentDTOCriteria {
    String name;
    ProducerEntity producer;
    AgeType ageType;
    Color color;
    Integer minCost;
    Integer maxCost;
    Integer count;
}
