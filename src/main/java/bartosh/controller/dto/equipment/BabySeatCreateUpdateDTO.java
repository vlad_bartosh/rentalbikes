package bartosh.controller.dto.equipment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class BabySeatCreateUpdateDTO extends EquipmentCreateUpdateDTO {
    public BabySeatCreateUpdateDTO(String name, Integer producer_id, Integer cost_id, Integer count, Integer pickupPointDTO_id) {
        super(name, producer_id, cost_id, count, pickupPointDTO_id);
    }
}
