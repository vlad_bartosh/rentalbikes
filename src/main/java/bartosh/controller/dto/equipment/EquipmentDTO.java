package bartosh.controller.dto.equipment;

import bartosh.controller.dto.BaseDTO;
import bartosh.controller.dto.CostDTO;
import bartosh.controller.dto.PickupPointDTO;
import bartosh.controller.dto.ProducerDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public abstract class EquipmentDTO extends BaseDTO {
    private String name;
    private ProducerDTO producer;
    private CostDTO cost;
    private Integer count;
    private PickupPointDTO pickupPointDTO;
}
