package bartosh.controller.dto.equipment;

import bartosh.controller.dto.CostDTO;
import bartosh.controller.dto.PickupPointDTO;
import bartosh.controller.dto.ProducerDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
public class BabySeatDTO extends EquipmentDTO {
    public BabySeatDTO(String name, ProducerDTO producer, CostDTO cost, Integer count, PickupPointDTO pickupPoint) {
        super( name, producer, cost, count, pickupPoint);
    }
}
