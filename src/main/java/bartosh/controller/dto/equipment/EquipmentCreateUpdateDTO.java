package bartosh.controller.dto.equipment;

import bartosh.controller.dto.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = false)
@Data
@NoArgsConstructor
@AllArgsConstructor
abstract class EquipmentCreateUpdateDTO extends BaseDTO {
    private String name;
    private Integer producer_id;
    private Integer cost_id;
    private Integer count;
    private Integer pickupPointDTO_id;
}
