package bartosh.controller.dto.equipment;

import bartosh.repository.entity.AgeType;
import bartosh.repository.entity.Color;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HelmetCreateUpdateDTO extends EquipmentCreateUpdateDTO {

    private AgeType ageType;
    private Color color;

    public HelmetCreateUpdateDTO(String name, Integer producer_id, Integer cost_id, Integer count, Integer pickupPoint_id, AgeType ageType, Color color) {
        super(name, producer_id, cost_id, count, pickupPoint_id);
        this.ageType = ageType;
        this.color = color;
    }
}
