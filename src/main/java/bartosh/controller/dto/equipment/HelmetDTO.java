package bartosh.controller.dto.equipment;

import bartosh.controller.dto.CostDTO;
import bartosh.controller.dto.PickupPointDTO;
import bartosh.controller.dto.ProducerDTO;
import bartosh.repository.entity.AgeType;
import bartosh.repository.entity.Color;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HelmetDTO extends EquipmentDTO {
    private AgeType ageType;
    private Color color;

    public HelmetDTO(String name, ProducerDTO producer, CostDTO cost, Integer count, PickupPointDTO pickupPoint, AgeType ageType, Color color) {
        super(name, producer, cost, count, pickupPoint);
        this.ageType = ageType;
        this.color = color;
    }
}
