package bartosh.controller.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;


@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PickupPointDTO extends BaseDTO {
    @NotNull
    private String name;
    @NotNull
    private String address;
    @NotNull
    private String latitude;
    @NotNull
    private String longtitude;
}
