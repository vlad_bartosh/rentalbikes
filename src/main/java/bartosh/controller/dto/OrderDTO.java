package bartosh.controller.dto;

import bartosh.controller.dto.equipment.EquipmentDTO;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderDTO extends BaseDTO {
    private PickupPointDTO pickupPoint;
    private PickupPointDTO pickoffPoint;
    @JsonFormat
            (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date rentFrom;
    @JsonFormat
            (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date rentTill;
    @NotNull
    private List<BikeDTO> bikes;
    private List<EquipmentDTO> equipments;
    @NotNull
    private UserDTO user;
    private BigDecimal finalCost;
    private Integer discount;
}
