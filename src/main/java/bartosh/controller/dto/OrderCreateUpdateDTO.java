package bartosh.controller.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderCreateUpdateDTO extends BaseDTO {
    private Integer pickupPoint_id;
    private Integer pickoffPoint_id;
    @JsonFormat
            (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date rentFrom;
    @JsonFormat
            (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date rentTill;
    private List<Integer> bikes_id;
    private Integer helmet_id;
    private Integer basket_id;
    private Integer babySeat_id;
    private Integer user_id;
    private Integer finalCost;
    private Integer discount;
}
