package bartosh.controller.dto;

import bartosh.repository.entity.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO extends BaseDTO {
    private String login; // TODO: 18.12.2017 validate all DTO's 
    private String email;
    private String telephone;
    private String password;
    private Set<Role> roles;
}
