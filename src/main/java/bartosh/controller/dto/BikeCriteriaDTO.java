package bartosh.controller.dto;

import bartosh.repository.entity.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class BikeCriteriaDTO {
    private String producer;
    private TypeBike typeBike;
    private Color color;
    private Status status;
    private Sex sex;
    private AgeType ageType;
    private String pickupPoint_id;
    @JsonFormat
            (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date rentFrom;
    @JsonFormat
            (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private Date rentTill;
    @Min(0)
    @NotNull(message = "minCost should not be null")
    private Integer minCost;
    @Min(0)
    @NotNull(message = "maxCost should not be null")
    private Integer maxCost;
    private Integer offset;
    private Integer limit;
}
