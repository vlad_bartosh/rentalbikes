package bartosh.controller.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProducerDTO {
    @Min(0)
    private Integer id;
    @NotNull
    private String name;
    @NotNull
    private String address;
}
