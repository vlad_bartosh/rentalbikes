package bartosh.controller;

import bartosh.controller.dto.equipment.BasketCreateUpdateDTO;
import bartosh.controller.dto.equipment.BasketDTO;
import bartosh.controller.dto.equipment.EquipmentDTOCriteria;

import java.util.List;

/**
 * IBasketController - class-controller that represent endpoints for Basket entity
 */
public interface IBasketController extends IBaseController<BasketDTO, BasketCreateUpdateDTO> {
    /**
     * Get BasketDTO from database by criteria
     *
     * @param equipmentDTOCriteria - criteria for Equipment
     * @return list of {@link BasketDTO}
     */
    List<BasketDTO> getAll(EquipmentDTOCriteria equipmentDTOCriteria);
}
