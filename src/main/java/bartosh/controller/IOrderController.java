package bartosh.controller;


import bartosh.controller.dto.OrderCreateUpdateDTO;
import bartosh.controller.dto.OrderDTO;

import java.util.List;

/**
 * IOrderController - class-controller that represent endpoints of Order entity
 */

public interface IOrderController extends IBaseController<OrderDTO, OrderCreateUpdateDTO> {
    /**
     * Get list of OrderDTO by User id
     *
     * @param user_id - id of User
     * @return list of {@link OrderDTO}
     */
    List<OrderDTO> getAll(Integer user_id);

    /**
     * Sends letters to employees
     *
     * @param orderDTO - OrderDTO that represent information in letter
     * @return - order that sends to employee
     */
    OrderDTO sendEmailToEmployee(OrderDTO orderDTO);
}
