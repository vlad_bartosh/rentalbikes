-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: bikerental
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*todo  remove unused comments*/
--
-- Table structure for table `appuser`
--


DROP TABLE IF EXISTS app_user;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appuser` (
  `id`        INT(11)      NOT NULL AUTO_INCREMENT,
  `login`     VARCHAR(45)           DEFAULT NULL,
  `email`     VARCHAR(45)           DEFAULT NULL,
  `password`  VARCHAR(100) NOT NULL,
  `telephone` VARCHAR(20)           DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_uindex` (`id`)
    <<<<<< < HEAD
) ENGINE =InnoDB AUTO_INCREMENT = 41 DEFAULT CHARSET = utf8;
=======
) ENGINE =InnoDB AUTO_INCREMENT =44 DEFAULT CHARSET =utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

>>>>>>> dev
--
-- Dumping data for table `appuser`
--

LOCK TABLES `appuser` WRITE;
<<<<<<< HEAD
INSERT INTO `appuser` VALUES (38, 'vlad', 'vlad@gmail.com', '$2a$10$jvu5DZCeFEgiuKMl9fftE.MC5dsd4JHd8ZjV4yegHBsRR92LeVUqW'), (39, 'bery', 'bery@mail.ru', '$2a$10$4PFMvep7Hf83fM.Kv4EkOuCp9Fl2BWoDPQ1P8Up3fOV7kXR.aEHwm'), (40, 'test', 'vlad.bartosh15@gmail.com', '$2a$10$5ZhfwCDlMsYwj9oAyyFGq.aRySmDQD/FCvA7XvQN/lMbEzQ8bxC.W');
=======
/*!40000 ALTER TABLE `appuser` DISABLE KEYS */;
INSERT INTO app_user
VALUES (43, 'Fox', 'fox@mail.ru', '$2a$10$RxZf8ilUyIpJwL29zXQH7.djx14uvFgsGtndCHQ5Pv6EyDZ4xrPdW', '375294567829');
/*!40000 ALTER TABLE `appuser` ENABLE KEYS */;
>>>>>>> dev
UNLOCK TABLES;

--
-- Table structure for table `bikes`
--

DROP TABLE IF EXISTS `bikes`;
CREATE TABLE `bikes` (
  `id`             INT(11)                                                                              NOT NULL AUTO_INCREMENT,
  `type`           ENUM ('ROAD', 'TOURING', 'HYBRID', 'CITY', 'MOUNTAIN')                               NOT NULL,
  `sex`            ENUM ('MALE', 'FEMALE')                                                              NOT NULL,
  `pickupPoint_id` INT(11)                                                                              NOT NULL,
  `ageType`        ENUM ('CHILD', 'TEENAGER', 'ADULT')                                                  NOT NULL,
  `cost_id`        INT(11)                                                                              NOT NULL,
  `producer_id`    INT(11)                                                                              NOT NULL,
  `status`         ENUM ('AVAILABLE_TO_RENT', 'UNAVAILABLE_TO_RENT', 'RENTED')                          NOT NULL,
  `color`          ENUM ('YELLOW', 'BLACK', 'WHITE', 'BLUE', 'RED', 'ORANGE', 'PINK', 'BROWN', 'GREEN') NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bikes_cost_id_fk` (`cost_id`),
  KEY `bikes_pickuppoint_id_fk` (`pickupPoint_id`),
  KEY `bikes_producer_id_fk` (`producer_id`),
  CONSTRAINT `FKiah81o8ot109pf405fr4wa6gm` FOREIGN KEY (`pickupPoint_id`) REFERENCES `pickuppoint` (`id`),
  CONSTRAINT `bikes_cost_id_fk` FOREIGN KEY (`cost_id`) REFERENCES `cost` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `bikes_pickuppoint_id_fk` FOREIGN KEY (`pickupPoint_id`) REFERENCES `pickuppoint` (`id`),
  CONSTRAINT `bikes_producer_id_fk` FOREIGN KEY (`producer_id`) REFERENCES `producer` (`id`)
    <<<<<< < HEAD
) ENGINE =InnoDB AUTO_INCREMENT = 25 DEFAULT CHARSET = utf8;
=======
) ENGINE =InnoDB AUTO_INCREMENT =39 DEFAULT CHARSET =utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
>>>>>>> dev

--
-- Dumping data for table `bikes`
--

LOCK TABLES `bikes` WRITE;
<<<<<<< HEAD
INSERT INTO `bikes` VALUES (1, 'ROAD', 'FEMALE', 1, 'CHILD', 1, 1, 'AVAILABLE_TO_RENT', 'YELLOW'), (2, 'CITY', 'MALE', 2, 'CHILD', 1, 4, 'AVAILABLE_TO_RENT', 'GREEN'), (3, 'HYBRID', 'FEMALE', 3, 'ADULT', 2, 1, 'UNAVAILABLE_TO_RENT', 'PINK'), (10, 'ROAD', 'FEMALE', 2, 'TEENAGER', 3, 5, 'RENTED', 'RED'), (11, 'ROAD', 'FEMALE', 1, 'CHILD', 1, 4, 'AVAILABLE_TO_RENT', 'WHITE'), (16, 'ROAD', 'FEMALE', 1, 'ADULT', 2, 1, 'AVAILABLE_TO_RENT', 'BLUE'), (17, 'HYBRID', 'MALE', 3, 'ADULT', 2, 5, 'AVAILABLE_TO_RENT', 'YELLOW'), (18, 'MOUNTAIN', 'MALE', 3, 'TEENAGER', 3, 13, 'AVAILABLE_TO_RENT', 'ORANGE'), (19, 'TOURING', 'FEMALE', 3, 'ADULT', 2, 12, 'AVAILABLE_TO_RENT', 'BLACK'), (20, 'ROAD', 'FEMALE', 3, 'CHILD', 1, 4, 'AVAILABLE_TO_RENT', 'BLACK'), (21, 'ROAD', 'MALE', 3, 'ADULT', 2, 6, 'AVAILABLE_TO_RENT', 'WHITE'), (22, 'CITY', 'MALE', 3, 'TEENAGER', 3, 7, 'AVAILABLE_TO_RENT', 'RED'), (23, 'CITY', 'FEMALE', 2, 'ADULT', 2, 2, 'AVAILABLE_TO_RENT', 'GREEN'), (24, 'ROAD', 'MALE', 3, 'CHILD', 1, 8, 'AVAILABLE_TO_RENT', 'BLUE');
=======
/*!40000 ALTER TABLE `bikes` DISABLE KEYS */;
INSERT INTO `bikes` VALUES (1, 'ROAD', 'FEMALE', 1, 'CHILD', 1, 1, 'AVAILABLE_TO_RENT', 'YELLOW'),
  (2, 'CITY', 'MALE', 2, 'CHILD', 1, 4, 'AVAILABLE_TO_RENT', 'GREEN'),
  (3, 'HYBRID', 'FEMALE', 3, 'ADULT', 2, 1, 'UNAVAILABLE_TO_RENT', 'PINK'),
  (10, 'ROAD', 'FEMALE', 2, 'TEENAGER', 3, 5, 'RENTED', 'RED'),
  (11, 'ROAD', 'FEMALE', 1, 'CHILD', 1, 4, 'AVAILABLE_TO_RENT', 'WHITE'),
  (16, 'ROAD', 'FEMALE', 1, 'ADULT', 2, 1, 'AVAILABLE_TO_RENT', 'BLUE'),
  (17, 'HYBRID', 'MALE', 3, 'ADULT', 2, 5, 'AVAILABLE_TO_RENT', 'YELLOW'),
  (18, 'MOUNTAIN', 'MALE', 3, 'TEENAGER', 3, 13, 'AVAILABLE_TO_RENT', 'ORANGE'),
  (19, 'TOURING', 'FEMALE', 3, 'ADULT', 2, 12, 'AVAILABLE_TO_RENT', 'BLACK'),
  (20, 'ROAD', 'FEMALE', 3, 'CHILD', 1, 4, 'AVAILABLE_TO_RENT', 'BLACK'),
  (21, 'ROAD', 'MALE', 3, 'ADULT', 2, 6, 'AVAILABLE_TO_RENT', 'WHITE'),
  (22, 'CITY', 'MALE', 3, 'TEENAGER', 3, 7, 'AVAILABLE_TO_RENT', 'RED'),
  (23, 'CITY', 'FEMALE', 2, 'ADULT', 2, 2, 'AVAILABLE_TO_RENT', 'GREEN'),
  (24, 'ROAD', 'MALE', 3, 'CHILD', 1, 8, 'AVAILABLE_TO_RENT', 'BLUE'),
  (25, 'MOUNTAIN', 'MALE', 1, 'ADULT', 2, 4, 'AVAILABLE_TO_RENT', 'YELLOW'),
  (26, 'CITY', 'FEMALE', 1, 'ADULT', 2, 7, 'AVAILABLE_TO_RENT', 'BLACK'),
  (27, 'HYBRID', 'MALE', 2, 'CHILD', 1, 4, 'AVAILABLE_TO_RENT', 'GREEN'),
  (28, 'ROAD', 'MALE', 1, 'ADULT', 2, 2, 'AVAILABLE_TO_RENT', 'BLACK'),
  (29, 'CITY', 'MALE', 1, 'ADULT', 2, 4, 'AVAILABLE_TO_RENT', 'ORANGE'),
  (30, 'MOUNTAIN', 'FEMALE', 1, 'CHILD', 1, 7, 'AVAILABLE_TO_RENT', 'BLUE'),
  (31, 'ROAD', 'FEMALE', 1, 'ADULT', 2, 6, 'AVAILABLE_TO_RENT', 'GREEN'),
  (32, 'HYBRID', 'MALE', 2, 'TEENAGER', 3, 8, 'AVAILABLE_TO_RENT', 'GREEN'),
  (33, 'CITY', 'MALE', 1, 'CHILD', 1, 14, 'AVAILABLE_TO_RENT', 'BLACK'),
  (34, 'HYBRID', 'FEMALE', 1, 'ADULT', 2, 13, 'AVAILABLE_TO_RENT', 'BLUE'),
  (35, 'MOUNTAIN', 'MALE', 1, 'TEENAGER', 3, 14, 'AVAILABLE_TO_RENT', 'ORANGE'),
  (36, 'ROAD', 'MALE', 1, 'ADULT', 7, 15, 'AVAILABLE_TO_RENT', 'YELLOW'),
  (37, 'HYBRID', 'MALE', 1, 'ADULT', 7, 5, 'AVAILABLE_TO_RENT', 'WHITE'),
  (38, 'TOURING', 'FEMALE', 1, 'CHILD', 1, 16, 'AVAILABLE_TO_RENT', 'GREEN');
/*!40000 ALTER TABLE `bikes` ENABLE KEYS */;
>>>>>>> dev
UNLOCK TABLES;

--
-- Table structure for table `cost`
--

DROP TABLE IF EXISTS `cost`;
CREATE TABLE `cost` (
  `id`        INT(11)                                                                                                                                      NOT NULL AUTO_INCREMENT,
  `cost`      INT(11)                                                                                                                                      NOT NULL,
  `type_cost` ENUM ('RENT_CHILD_HELMET', 'RENT_CHILD_BIKE', 'RENT_TEENAGER_BIKE', 'RENT_ADULT_BIKE', 'RENT_ADULT_HELMET', 'RENT_BASKET', 'RENT_BABY_SEAT') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cost_id_uindex` (`id`)
) ENGINE =InnoDB AUTO_INCREMENT = 10 DEFAULT CHARSET = utf8;

--
-- Dumping data for table `cost`
--

LOCK TABLES `cost` WRITE;
/*!40000 ALTER TABLE `cost` DISABLE KEYS */;
INSERT INTO `cost`
VALUES (1, 3, 'RENT_CHILD_BIKE'), (2, 5, 'RENT_ADULT_BIKE'), (3, 8, 'RENT_TEENAGER_BIKE'), (4, 3, 'RENT_ADULT_HELMET'),
  (5, 2, 'RENT_BABY_SEAT'), (7, 10, 'RENT_ADULT_BIKE'), (8, 2, 'RENT_CHILD_BIKE'), (9, 1, 'RENT_BASKET');
/*!40000 ALTER TABLE `cost` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipment`
--

DROP TABLE IF EXISTS `equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipment` (
  `id`             INT(11)     NOT NULL                AUTO_INCREMENT,
  `name`           VARCHAR(45) NOT NULL,
  `typeEquipment`  VARCHAR(45) NOT NULL,
  `count`          INT(11)     NOT NULL,
  `cost_id`        INT(11)     NOT NULL,
  `pickupPoint_id` INT(11)     NOT NULL,
  `ageType`        ENUM ('CHILD', 'TEENAGER', 'ADULT') DEFAULT NULL,
  `color`          VARCHAR(45)                         DEFAULT NULL,
  `producer_id`    INT(11)     NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Equipment_id_uindex` (`id`),
  KEY `equipment_cost_id_fk` (`cost_id`),
  KEY `equipment_producer_id_fk` (`producer_id`),
  KEY `FK9clvp7epk1jsqhhp5lutary1y` (`pickupPoint_id`),
  CONSTRAINT `FK9clvp7epk1jsqhhp5lutary1y` FOREIGN KEY (`pickupPoint_id`) REFERENCES `pickuppoint` (`id`),
  CONSTRAINT `equipment_cost_id_fk` FOREIGN KEY (`cost_id`) REFERENCES `cost` (`id`),
  CONSTRAINT `equipment_pickuppoint_id_fk` FOREIGN KEY (`pickupPoint_id`) REFERENCES `pickuppoint` (`id`),
  CONSTRAINT `equipment_producer_id_fk` FOREIGN KEY (`producer_id`) REFERENCES `producer` (`id`)
) ENGINE =InnoDB AUTO_INCREMENT = 23 DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipment`
--

LOCK TABLES `equipment` WRITE;
/*!40000 ALTER TABLE `equipment` DISABLE KEYS */;
INSERT INTO `equipment` VALUES (10, 'Helmet', 'helmet', 23, 4, 1, 'ADULT', 'RED', 1),
  (11, 'BabySeat', 'babySeat', 12, 5, 2, 'ADULT', 'BLACK', 3), (12, 'Basket', 'basket', 8, 2, 1, 'ADULT', 'ORANGE', 2),
  (13, 'Helmet', 'helmet', 32, 4, 3, 'TEENAGER', 'PINK', 2), (14, 'Helmet', 'helmet', 21, 3, 2, 'ADULT', 'ORANGE', 3),
  (15, 'Helmet', 'helmet', 12, 3, 1, 'CHILD', 'PINK', 3), (16, 'Basket', 'basket', 3, 2, 1, 'CHILD', 'BLACK', 18),
  (17, 'BabySeat', 'babySeat', 23, 5, 1, 'ADULT', 'BLUE', 19),
  (18, 'BabySeat', 'babySeat', 25, 5, 1, 'ADULT', 'GREEN', 17),
  (19, 'BabySeat', 'babySeat', 12, 5, 2, 'ADULT', 'ORANGE', 18),
  (20, 'BabtSeat', 'babySeat', 12, 5, 1, 'CHILD', 'BLACK', 19),
  (21, 'Helmet', 'helmet', 17, 3, 1, 'ADULT', 'GREEN', 18), (22, 'Basket', 'basket', 20, 2, 1, 'ADULT', 'BLACK', 17);
/*!40000 ALTER TABLE `equipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hibernate_sequence` (
  `next_val` BIGINT(20) DEFAULT NULL
) ENGINE =MyISAM DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (17);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_bikes`
--

DROP TABLE IF EXISTS `orders_bikes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_bikes` (
  `order_id` INT(11) NOT NULL,
  `bike_id`  INT(11) NOT NULL,
  KEY `orders_bikes_order_id_fk` (`order_id`),
  KEY `orders_bikes_bikes_id_fk` (`bike_id`),
  CONSTRAINT `FK7lbuta7lt6iqnmct6f66r1pal` FOREIGN KEY (`order_id`) REFERENCES user_order (`id`),
  CONSTRAINT `orders_bikes_bikes_id_fk` FOREIGN KEY (`bike_id`) REFERENCES `bikes` (`id`)
) ENGINE =InnoDB DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_bikes`
--

LOCK TABLES `orders_bikes` WRITE;
/*!40000 ALTER TABLE `orders_bikes` DISABLE KEYS */;
INSERT INTO `orders_bikes` VALUES (74, 11), (75, 1), (76, 17);
/*!40000 ALTER TABLE `orders_bikes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders_equipment`
--

DROP TABLE IF EXISTS `orders_equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders_equipment` (
  `order_id`     INT(11) NOT NULL,
  `equipment_id` INT(11) DEFAULT NULL,
  KEY `orders_equipment_order_id_fk` (`order_id`),
  KEY `orders_equipment_equipment_id_fk` (`equipment_id`),
  CONSTRAINT `FK4wkdt9qa8eu3nbxmfvlt9gapf` FOREIGN KEY (`order_id`) REFERENCES user_order (`id`),
  CONSTRAINT `orders_equipment_equipment_id_fk` FOREIGN KEY (`equipment_id`) REFERENCES `equipment` (`id`),
  CONSTRAINT `orders_equipment_order_id_fk` FOREIGN KEY (`order_id`) REFERENCES user_order (`id`)
) ENGINE =InnoDB DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders_equipment`
--

LOCK TABLES `orders_equipment` WRITE;
/*!40000 ALTER TABLE `orders_equipment` DISABLE KEYS */;
INSERT INTO `orders_equipment` VALUES (75, 10), (75, 12), (75, 17), (76, 13);
/*!40000 ALTER TABLE `orders_equipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pickuppoint`
--

DROP TABLE IF EXISTS `pickuppoint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pickuppoint` (
  `id`         INT(11)     NOT NULL AUTO_INCREMENT,
  `name`       VARCHAR(45) NOT NULL,
  `address`    VARCHAR(45) NOT NULL,
  `latitude`   VARCHAR(45) NOT NULL,
  `longtitude` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pickupPoint_id_uindex` (`id`)
) ENGINE =InnoDB AUTO_INCREMENT = 8 DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pickuppoint`
--

LOCK TABLES `pickuppoint` WRITE;
/*!40000 ALTER TABLE `pickuppoint` DISABLE KEYS */;
INSERT INTO `pickuppoint`
VALUES (1, 'Stepyanka', 'kolasa,28', '132', '1231'), (2, 'Malina', 'Dzerzhinskogo,34', '1312', '5674'),
  (3, 'Pobedy', 'Nezavisimosty sq., 17', '4132', '1433'), (4, 'Lenina', 'Maykovskogo,28', '132', '1231'),
  (5, 'Sportivnaya', 'Prityckaga str., 34', '1323', '23554'), (6, 'Nemiga', 'Nemiga str., 25/1', '15332', '23435'),
  (7, 'Sportivnaya', 'Prityckaga str., 34', '1323', '23554');
/*!40000 ALTER TABLE `pickuppoint` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producer`
--

DROP TABLE IF EXISTS `producer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producer` (
  `id`      INT(11)     NOT NULL AUTO_INCREMENT,
  `name`    VARCHAR(45) NOT NULL,
  `address` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `producer_id_uindex` (`id`)
) ENGINE =InnoDB AUTO_INCREMENT = 21 DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producer`
--

LOCK TABLES `producer` WRITE;
/*!40000 ALTER TABLE `producer` DISABLE KEYS */;
INSERT INTO `producer`
VALUES (1, 'Aist', 'Minsk,Independence sq., 25'), (2, 'Bayer', 'Germany,Berlin'), (3, 'Zybr', 'Belarus,Brest'),
  (4, 'Stels', 'Russia'), (5, 'MTL', 'Poland'), (6, 'Trek', 'USA'), (7, 'GT', 'USA'), (8, 'Felt', 'USA'),
  (9, 'Jeep', 'USA'), (10, 'LTD', 'Germany'), (11, 'Focus', 'Germany'), (12, 'Rover', 'Poland'),
  (13, 'Kross', 'Poland'), (14, 'Look', 'France'), (15, 'LS2', 'China'), (16, 'Yamaha', 'Japan'),
  (17, 'Acerbis', 'Italy'), (18, 'AFX', 'USA'), (19, 'A-Pro', 'Italy'), (20, 'Arai', 'Japan');
/*!40000 ALTER TABLE `producer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `user_id` INT(11)                                                         NOT NULL,
  `role`    ENUM ('ROLE_ADMIN', 'ROLE_USER', 'ROLE_WORKER', 'ROLE_VISITOR') NOT NULL,
  `id`      INT(11)                                                         NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_role_id_uindex` (`id`),
  KEY `FKbt47pk75ihgi1fpwbpqrmsh4l` (`user_id`),
  CONSTRAINT `FKbt47pk75ihgi1fpwbpqrmsh4l` FOREIGN KEY (`user_id`) REFERENCES app_user (`id`),
  CONSTRAINT `user_role_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES app_user (`id`)
) ENGINE =InnoDB AUTO_INCREMENT = 109 DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (43, 'ROLE_USER', 108);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userorder`
--

DROP TABLE IF EXISTS user_order;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userorder` (
  `id`         INT(11)  NOT NULL AUTO_INCREMENT,
  `pickup_id`  INT(11)  NOT NULL,
  `pickoff_id` INT(11)  NOT NULL,
  `rentFrom`   DATETIME NOT NULL,
  `rentTill`   DATETIME NOT NULL,
  `user_id`    INT(11)  NOT NULL,
  `discount`   INT(11)  NOT NULL,
  `finalCost`  INT(11)  NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1bqh6gjshvlmkiufv82fn6oqh` (`pickoff_id`),
  KEY `FKjk9j9olimt3y5tg57n2pps366` (`pickup_id`),
  KEY `FKqsk5i6pmdvwuvd942xa91huye` (`user_id`),
  CONSTRAINT `FK1bqh6gjshvlmkiufv82fn6oqh` FOREIGN KEY (`pickoff_id`) REFERENCES `pickuppoint` (`id`),
  CONSTRAINT `FKjk9j9olimt3y5tg57n2pps366` FOREIGN KEY (`pickup_id`) REFERENCES `pickuppoint` (`id`),
  CONSTRAINT `FKqsk5i6pmdvwuvd942xa91huye` FOREIGN KEY (`user_id`) REFERENCES app_user (`id`),
  CONSTRAINT `userorder_pickoffpoint_id_fk` FOREIGN KEY (`pickoff_id`) REFERENCES `pickuppoint` (`id`),
  CONSTRAINT `userorder_pickuppoint_id_fk` FOREIGN KEY (`pickup_id`) REFERENCES `pickuppoint` (`id`)
) ENGINE =InnoDB AUTO_INCREMENT = 79 DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userorder`
--

LOCK TABLES `userorder` WRITE;
/*!40000 ALTER TABLE `userorder` DISABLE KEYS */;
INSERT INTO user_order VALUES (74, 1, 1, '2017-12-19 14:26:00', '2017-12-19 16:21:00', 43, 0, 340),
  (75, 1, 1, '2017-12-20 16:50:00', '2017-12-20 23:50:00', 43, 0, 31),
  (76, 3, 1, '2017-12-20 20:52:00', '2017-12-20 23:52:00', 43, 0, 18);
/*!40000 ALTER TABLE `userorder` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-20 14:39:02
